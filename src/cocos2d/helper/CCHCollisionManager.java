/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.helper;

import cocos2d.common.CCRect;
import cocos2d.common.schedule.CCSchedule;
import cocos2d.node.CCNode;
import cocos2d.node.CCNodeDelegate;
import java.util.Vector;

/**
 *
 * @author KieuAnh
 */
public class CCHCollisionManager implements CCNodeDelegate, CCSchedule
{
    private final Vector collideables = new Vector();
    
    public void onChildAdded(CCNode child)
    {
        if (child instanceof CCHCollideable)
        {
            this.collideables.addElement(child);
        }
    }

    public void onChildRemoved(CCNode child)
    {
        this.collideables.removeElement(child);
    }

    public void run(long dt)
    {
        int n = this.collideables.size();
        for (int i = 0; i < n; i++) {
            Object iCollObj = this.collideables.elementAt(i);
            
            if (iCollObj instanceof CCHCollideable)
            {
                for (int j = i + 1; j < n; j++) {
                    Object jCollObj = this.collideables.elementAt(j);
                    if (jCollObj instanceof CCHCollideable)
                    {
                        CCHCollideable iColl = (CCHCollideable) iCollObj;
                        CCHCollideable jColl = (CCHCollideable) jCollObj;
                        
                        // -1 is haven't test collide yet
                        // 0 is not collided
                        // 1 is they're collided
                        int isCollided = -1;
                        if ((iColl.collideableMask() & jColl.collideMask()) != 0)
                        {
                            isCollided = this.checkCollide(iColl, jColl);
                            
                            if (isCollided == 1)
                            {
                                iColl.collide(jColl);
                            }
                        }
                        
                        if ((jColl.collideableMask() & iColl.collideMask()) != 0)
                        {
                            if (isCollided == -1)
                            {
                                isCollided = this.checkCollide(jColl, iColl);
                            }
                            
                            if (isCollided == 1)
                            {
                                jColl.collide(iColl);
                            }
                        }
                    }
                }
            }
        }
    }
    
    private int checkCollide(CCNode a, CCNode b)
    {
        CCRect aBound = a.getBound();
        CCRect bBound = b.getBound();
        
        return aBound.intersect(bBound)?1:0;
    }
}