/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.helper;

import cocos2d.node.CCBaseNode;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author KieuAnh
 */
public class CCHScreenLog extends CCBaseNode
{
    public static final StringBuffer logger = new StringBuffer();
    
    public void draw(Graphics g, long dt)
    {
        String log = logger.toString();
        if (log.length() > 0)
        {
            g.drawString(log, 0, 0, 0);
        }
        
        super.draw(g, dt);
        
        logger.delete(0, logger.length());
    }
    
    private CCHScreenLog()
    {
        super();
    }
    
    private static final CCHScreenLog inst = new CCHScreenLog();
    
    public static CCHScreenLog inst()
    {
        return inst;
    }
}
