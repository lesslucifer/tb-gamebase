/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.helper;

import cocos2d.node.CCNode;

/**
 *
 * @author KieuAnh
 */
public interface CCHCollideable extends CCNode
{
    public static final int COLLIDE_MASK_1 = 0x1;
    public static final int COLLIDE_MASK_2 = 0x1<<1;
    public static final int COLLIDE_MASK_3 = 0x1<<2;
    public static final int COLLIDE_MASK_4 = 0x1<<3;
    public static final int COLLIDE_MASK_5 = 0x1<<4;
    public static final int COLLIDE_MASK_6 = 0x1<<5;
    public static final int COLLIDE_MASK_7 = 0x1<<6;
    public static final int COLLIDE_MASK_8 = 0x1<<7;
    public static final int COLLIDE_MASK_9 = 0x1<<8;
    public static final int COLLIDE_MASK_10 = 0x1<<9;
    public static final int COLLIDE_MASK_11 = 0x1<<10;
    public static final int COLLIDE_MASK_12 = 0x1<<11;
    public static final int COLLIDE_MASK_13 = 0x1<<12;
    public static final int COLLIDE_MASK_14 = 0x1<<13;
    public static final int COLLIDE_MASK_15 = 0x1<<14;
    public static final int COLLIDE_MASK_16 = 0x1<<15;
    public static final int COLLIDE_MASK_17 = 0x1<<16;
    public static final int COLLIDE_MASK_18 = 0x1<<17;
    public static final int COLLIDE_MASK_19 = 0x1<<18;
    public static final int COLLIDE_MASK_20 = 0x1<<19;
    public static final int COLLIDE_MASK_21 = 0x1<<20;
    public static final int COLLIDE_MASK_22 = 0x1<<21;
    public static final int COLLIDE_MASK_23 = 0x1<<22;
    public static final int COLLIDE_MASK_24 = 0x1<<23;
    public static final int COLLIDE_MASK_25 = 0x1<<24;
    public static final int COLLIDE_MASK_26 = 0x1<<25;
    public static final int COLLIDE_MASK_27 = 0x1<<26;
    public static final int COLLIDE_MASK_28 = 0x1<<27;
    public static final int COLLIDE_MASK_29 = 0x1<<28;
    public static final int COLLIDE_MASK_30 = 0x1<<29;
    public static final int COLLIDE_MASK_31 = 0x1<<30;
    public static final int COLLIDE_MASK_32 = 0x1<<31;
    
    public static final int COLLIDE_MASK_PLAYER = COLLIDE_MASK_1;
    public static final int COLLIDE_MASK_AI = COLLIDE_MASK_2;

    public int collideMask();
    public int collideableMask();
    
    public void collide(CCHCollideable collided);
}