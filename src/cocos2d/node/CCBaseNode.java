/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.node;

import cocos2d.common.CCPoint;
import cocos2d.common.CCRect;
import cocos2d.common.CCSize;
import cocos2d.common.schedule.CCBaseScheduler;
import cocos2d.common.schedule.CCSchedule;
import cocos2d.common.schedule.CCScheduler;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;
import tbgb.log.TBLog;

/**
 *
 * @author KieuAnh
 */
public class CCBaseNode implements CCNode
{
    private final CCRect bound;
    private final Vector childs;
    private CCNode parent;
    private final CCPoint anchor;
    private final CCScheduler scheduler;
    private boolean isFlexibled = false;
    private boolean visibled = true;
    private boolean dead = false;
    private Vector delegates = null;
    
    public CCBaseNode()
    {
        this.bound = new CCRect();
        this.childs = new Vector();
        this.parent = null;
        this.anchor = new CCPoint();
        this.scheduler = new CCBaseScheduler();  // base scheduler
    }

    // <editor-fold defaultstate="collapsed" desc="Scheduler">
    /**
     * @return the scheduler
     */
    public CCScheduler getScheduler() {
        return scheduler;
    }
    
    public void schedule(CCSchedule schedule) {
        this.getScheduler().schedule(schedule);
    }

    public void schedule(CCSchedule schedule, long time) {
        this.getScheduler().schedule(schedule, time);
    }

    public void schedule(CCSchedule schedule, long time, int p) {
        this.getScheduler().schedule(schedule, time, p);
    }

    public void unschedule(CCSchedule schedule) {
        this.getScheduler().unschedule(schedule);
    }

    public void run(long dt)
    {
        this.getScheduler().run(dt);
    }

    public void scheduleUpdate()
    {
        this.getScheduler().schedule(new CCSchedule() {

            public void run(long dt)
            {
                update(dt);
            }
        });
    }
    
    public void scheduleRemove()
    {
        this.getScheduler().schedule(new CCSchedule() {

            public void run(long dt)
            {
                Vector removed = new Vector();
                
                for (int i = 0; i < childs.size(); i++) {
                    Object childObj = childs.elementAt(i);
                    if (childObj instanceof CCNode)
                    {
                        CCNode child = (CCNode) childObj;
                        if (child.isDead())
                        {
                            removed.addElement(child);
                        }
                    }
                }
                
                for (int i = 0; i < removed.size(); i++)
                {
                    CCNode child = (CCNode) removed.elementAt(i);
                    removeChild(child);
                }
            }
        });
    }
    
    public void update(long dt)
    {
        
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Node">
    /**
     * @return the parent
     */
    public CCNode getParent() {
        return parent;
    }
   
    public void setParent(CCNode parent) {
        this.parent = parent;
    }
    
    /**
     * @return the children
     */
    public Vector getAllChildrens() {
        return childs;
    }
    
    public void addChild(CCNode child)
    {
        this.childs.addElement(child);
        child.setParent(this);
        
        this.updateVisibleSize();
        this.onAdded(child);
    }
    
    public void addSibling(CCNode sibling)
    {
        if (this.getParent() != null)
        {
            this.getParent().addChild(sibling);
        }
    }
    
    public void removeChild(CCNode child)
    {
        if (this.childs.removeElement(child))
        {
            child.setParent(null);
            this.onRemoved(child);
        }
        
        this.updateVisibleSize();
    }
    
    public void removeChildrens(Vector childs)
    {
        for (int i = 0; i < childs.size(); i++) {
            Object nodeObj = childs.elementAt(i);
            if (nodeObj instanceof CCNode)
            {
                this.removeChild((CCNode) nodeObj);
            }
        }
    }
    
    public void removeAllChildrens()
    {
        for (int i = 0; i < this.childs.size(); i++) {
            Object nodeObj = this.childs.elementAt(i);
            if (nodeObj instanceof CCNode)
            {
                CCNode childNode = (CCNode) nodeObj;
                childNode.setParent(null);
            }
        }
        this.childs.removeAllElements();
    }
    
    public void removeFromParent()
    {
        if (this.parent != null)
        {
            this.parent.removeChild(this);
        }
    }
    
    public boolean isChildOf(CCNode parent)
    {
        CCNode cParent = this.parent;
        while (cParent != null && cParent != parent) {
            cParent = cParent.getParent();
        }
        
        return (cParent != null);
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Transform">
    /**
     * @return the position
     */
    public CCPoint getPosition() {
        return this.getBound().getPosition();
    }

    /**
     * @return the anchor
     */
    public CCPoint getAnchor() {
        return anchor;
    }
    
    public CCPoint getGlobalPosition()
    {
        return this.globalFromLocal(this.anchor.negative());
    }
    
    public CCPoint globalFromLocal(CCPoint local)
    {
        CCPoint global = local.copy();
        
        CCNode cParent = this;
        while (cParent != null) {
            global.saAdd(cParent.getPosition());
            
            cParent = cParent.getParent();
        }
        
        return global;
    }
    
    public CCPoint localFromGlobal(CCPoint global)
    {
        CCPoint local = global.copy();
        
        CCNode cParent = this;
        while (cParent != null) {
            TBLog.log("local: " + local);
            local.saSub(cParent.getPosition());
            
            cParent = cParent.getParent();
        }
        
        return local;
    }
    
    public CCPoint parentFromGlobal(CCPoint global)
    {
        if (this.getParent() != null)
        {
            return this.getParent().localFromGlobal(global);
        }
        
        return global.copy();
    }
    
    public CCSize getContentSize() {
        return this.bound.getSize();
    }
    
    public CCRect getBound()
    {
        return this.bound;
    }
    
    public CCRect getGlobalBound()
    {
        return new CCRect(this.getGlobalPosition(), this.bound.getSize());
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Flexible">
    private void updateVisibleSize()
    {
        if(this.isFlexibled())
        {
            CCPoint topLeft = new CCPoint(), rightBottom = new CCPoint();
            for (int i = 0; i < this.getAllChildrens().size(); i++) {
                Object nodeObj = this.getAllChildrens().elementAt(i);
                if (nodeObj instanceof CCNode)
                {
                    CCNode child = (CCNode) nodeObj;
                    topLeft.X = Math.min(topLeft.X, child.getPosition().X);
                    topLeft.Y = Math.min(topLeft.Y, child.getPosition().Y);
                    rightBottom.X = Math.max(rightBottom.X, child.getPosition().X + child.getContentSize().X);
                    rightBottom.Y = Math.max(rightBottom.Y, child.getPosition().Y + child.getContentSize().Y);
                }
            }
            
            this.getContentSize().setWidth(rightBottom.X - topLeft.X);
            this.getContentSize().setHeight(rightBottom.Y - topLeft.Y);
        }
    }

    /**
     * @return the isFlexibled
     */
    
    public boolean isFlexibled() {
        return isFlexibled;
    }

    /**
     * @param isFlexibled the isFlexibled to set
     */
    
    public void setIsFlexibled(boolean isFlexibled) {
        if (this.isFlexibled != isFlexibled)
        {
            this.isFlexibled = isFlexibled;
            this.updateVisibleSize();
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Visible">
    
    public void draw(Graphics g, long dt)
    {
        if (this.isVisibled())
        {
            for (int i = 0; i < this.childs.size(); i++) {
                Object nodeObj = this.childs.elementAt(i);

                if (nodeObj instanceof CCNode)
                {
                    CCNode child = (CCNode) nodeObj;
                    child.draw(g, dt);
                }
            }
        }
    }
    
    /**
     * @return the visible
     */
    
    public boolean isVisibled() {
        return visibled;
    }

    /**
     * @param visibled the visible to set
     */
    
    public void setVisibled(boolean visibled) {
        this.visibled = visibled;
    }
    // </editor-fold>

    /**
     * @return the dead
     */
    public boolean isDead() {
        return dead;
    }

    /**
     * @param dead the dead to set
     */
    public void setDead(boolean dead) {
        this.dead = dead;
    }

    
    
    private void onAdded(CCNode child) {
        if (this.delegates == null)
        {
            return;
        }
        
        for (int i = 0; i < this.delegates.size(); ++i)
        {
            Object delObj = this.delegates.elementAt(i);
            if (delObj instanceof CCNodeDelegate)
            {
                CCNodeDelegate del = (CCNodeDelegate) delObj;
                del.onChildAdded(child);
            }
            else
            {
                this.delegates.removeElementAt(i);
                --i;
            }
        }
    }

    private void onRemoved(CCNode child) {
        if (this.delegates == null)
        {
            return;
        }
        
        for (int i = 0; i < this.delegates.size(); ++i)
        {
            Object delObj = this.delegates.elementAt(i);
            if (delObj instanceof CCNodeDelegate)
            {
                CCNodeDelegate del = (CCNodeDelegate) delObj;
                del.onChildRemoved(child);
            }
            else
            {
                this.delegates.removeElementAt(i);
                --i;
            }
        }
    }

    public void addDelegate(CCNodeDelegate del)
    {
        if (this.delegates == null)
        {
            this.delegates = new Vector();
        }
        
        this.delegates.addElement(del);
    }

    public void removeDelegate(CCNodeDelegate del)
    {
        if (this.delegates != null)
        {
            this.delegates.removeElement(del);
        }
    }
}