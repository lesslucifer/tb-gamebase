/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.node;

import cocos2d.common.CCPoint;
import cocos2d.common.CCRect;
import cocos2d.common.CCSize;
import cocos2d.common.schedule.CCSchedule;
import cocos2d.common.schedule.CCScheduler;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author KieuAnh
 */
public interface CCNode extends CCScheduler {
    CCPoint getAnchor();
    CCRect getBound();
    CCSize getContentSize();
    CCRect getGlobalBound();
    CCPoint getGlobalPosition();
    CCPoint getPosition();
    CCPoint globalFromLocal(CCPoint local);
    boolean isChildOf(CCNode parent);

    CCPoint localFromGlobal(CCPoint global);
    
    boolean isFlexibled();
    void setIsFlexibled(boolean isFlexibled);


    CCNode getParent();
    void setParent(CCNode parent);
    void addChild(CCNode child);
    void addSibling(CCNode sibling);
    Vector getAllChildrens();
    void removeAllChildrens();
    void removeChild(CCNode child);
    void removeChildrens(Vector childs);
    void removeFromParent();

    boolean isVisibled();
    void setVisibled(boolean visibled);
    void draw(Graphics g, long dt);
    void run(long dt);

    void schedule(CCSchedule schedule);
    void schedule(CCSchedule schedule, long time);
    void unschedule(CCSchedule schedule);
    
    void scheduleUpdate();
    void scheduleRemove();
    
    public boolean isDead();
    public void setDead(boolean dead);
    
    public void addDelegate(CCNodeDelegate del);
    public void removeDelegate(CCNodeDelegate del);
}
