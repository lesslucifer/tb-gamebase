/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.touch;

import cocos2d.common.CCPoint;
import com.nokia.mid.ui.multipointtouch.MultipointTouch;
import tbgb.gesturehandle.touch.TBTouch;

/**
 *
 * @author KieuAnh
 */
public class CCTouch
{
    public static final int TOUCH_RESULT_EXCLUSIVED = 2;
    public static final int TOUCH_RESULT_HANDLING = 1;
    public static final int TOUCH_RESULT_NONE = 0;
    
    private final int touchID;
    private final CCPoint current, last, begin;
    private final int lastState, currentState;
    
    public CCTouch(TBTouch touch)
    {
        this.touchID = touch.getTouchID();
        this.current = new CCPoint(touch.getCurrent());
        this.last = new CCPoint(touch.getLast());
        this.begin = new CCPoint(touch.getBegin());
        this.lastState = touch.getLastState();
        this.currentState = touch.getCurrentState();
    }
    
    public CCTouch(CCTouch touch)
    {
        this.touchID = touch.getTouchID();
        this.current = new CCPoint(touch.getCurrent());
        this.last = new CCPoint(touch.getLast());
        this.begin = new CCPoint(touch.getBegin());
        this.lastState = touch.getLastState();
        this.currentState = touch.getCurrentState();
    }
    
    public CCTouch copy()
    {
        return new CCTouch(this);
    }

    /**
     * @return the touchID
     */
    public int getTouchID() {
        return touchID;
    }

    /**
     * @return the current
     */
    public CCPoint getCurrent() {
        return current;
    }

    /**
     * @return the last
     */
    public CCPoint getLast() {
        return last;
    }

    /**
     * @return the begin
     */
    public CCPoint getBegin() {
        return begin;
    }

    /**
     * @return the state
     */
    public int getState() {
        return MultipointTouch.getState(touchID);
    }

    /**
     * @return the lastState
     */
    public int getLastState() {
        return lastState;
    }

    /**
     * @return the currentState
     */
    public int getCurrentState() {
        return currentState;
    }
    
    public CCPoint getDelta()
    {
        return current.sub(last);
    }

    public String toString() {
        return "CCTouch{" + "touchID=" + touchID + ", current=" + current + ", last=" + last + ", lastState=" + lastState + ", currentState=" + currentState + '}';
    }
}
