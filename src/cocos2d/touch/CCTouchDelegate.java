/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.touch;

import cocos2d.common.CCPoint;

/**
 *
 * @author KieuAnh
 */
public interface CCTouchDelegate
{
    void touchBegan(CCTouch touch);
    void touchMoved(CCTouch touch);
    void touchEnded(CCTouch touch);
}
