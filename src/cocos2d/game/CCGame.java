/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.game;

import cocos2d.common.CCRect;
import cocos2d.game.scene.CCSceneManager;
import com.nokia.mid.ui.multipointtouch.MultipointTouch;
import java.util.Vector;
import tbgb.BaseGame;
import tbgb.gesturehandle.TBTouchDispatcher;
import tbgb.gesturehandle.touch.TBTouchController;
import tbgb.process.render.RenderProcessManager;
import tbgb.process.update.UpdateGameProcessManager;

/**
 *
 * @author KieuAnh
 */
public class CCGame extends BaseGame
{
    private final TBTouchDispatcher tDispatcher = new TBTouchDispatcher();
    private CCSceneManager sceneManager;
    private CCRect windowBound;
    
    public void initialize()
    {
        super.initialize();
        
        this.windowBound = new CCRect(this.getGameBound());
        this.sceneManager = new CCSceneManager(this);
        this.addEntity(sceneManager);
    }

    protected void initProcessManagers(Vector procManagersContainer) {
        super.initProcessManagers(procManagersContainer);
        
        procManagersContainer.addElement(new UpdateGameProcessManager());
        procManagersContainer.addElement(new RenderProcessManager());
        
        TBTouchController tController = new TBTouchController();
        MultipointTouch.getInstance().addMultipointTouchListener(tController);
        tController.getDispatchers().addElement(this.getTouchDispatchers());
        this.getServices().put(TBTouchDispatcher.class, this.tDispatcher);
    }

    /**
     * @return the dispatchers
     */
    public TBTouchDispatcher getTouchDispatchers()
    {
        return tDispatcher;
    }

    /**
     * @return the sceneManager
     */
    public CCSceneManager getSceneManager() {
        return sceneManager;
    }

    /**
     * @return the windowBound
     */
    public CCRect getBound() {
        return this.windowBound;
    }
}