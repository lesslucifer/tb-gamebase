/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.game.scene;

import cocos2d.node.CCBaseNode;
import cocos2d.node.CCNode;
import cocos2d.touch.CCTouch;
import cocos2d.touch.CCTouchDelegate;
import com.nokia.mid.ui.multipointtouch.MultipointTouch;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;
import tbgb.gesturehandle.TBTouchTarget;
import tbgb.gesturehandle.touch.TBTouch;
import tbgb.process.render.RenderProcess;
import tbgb.process.update.UpdateProcess;

/**
 *
 * @author KieuAnh
 */
public class CCScene extends CCBaseNode implements UpdateProcess, RenderProcess
{
    public static final int Activated = 0;
    public static final int Deactivated = 1;
    public static final int Paused = 2;
    
    private boolean enabled = true;
    private String name;
    
    public void update(long eslapedGameTime)
    {
        this.run(eslapedGameTime);
        Vector allChils = this.getAllChildrens();
        
        for (int i = 0; i < allChils.size(); i++) {
            Object childObj = allChils.elementAt(i);
            if (childObj instanceof CCNode)
            {
                CCNode child = (CCNode) childObj;
                child.run(eslapedGameTime);
            }
        }
    }

    public void render(Graphics g, long eslapedGameTime)
    {
        Vector allChils = this.getAllChildrens();
        
        for (int i = 0; i < allChils.size(); i++) {
            Object childObj = allChils.elementAt(i);
            if (childObj instanceof CCNode)
            {
                CCNode child = (CCNode) childObj;
                child.draw(g, eslapedGameTime);
            }
        }
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    
    public int getState()
    {
        if (this.isEnabled() && this.isVisibled())
        {
            return CCScene.Activated;
        }
        else if (!this.isEnabled() && this.isVisibled())
        {
            return CCScene.Paused;
        }

        return CCScene.Deactivated;
    }
    
    public void setState(int value)
    {
        int oldState = this.getState();
        if (oldState == value)
            return;

        switch (value)
        {
            case CCScene.Activated:
                this.setEnabled(true);
                this.setVisibled(true);
                break;
            case CCScene.Paused:
                this.setEnabled(false);
                this.setVisibled(true);
                break;
            case CCScene.Deactivated:
                this.setEnabled(false);
                this.setVisibled(false);
                break;
            default:
                break;
        }
        
        this.OnStateChanged(oldState);
    }

    protected void OnStateChanged(int oldState)
    {
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public int hasTouch(CCTouch touch)
    {
        Vector allChils = this.getAllChildrens();
        for (int i = 0; i < allChils.size(); i++)
        {
            Object nodeObj = allChils.elementAt(i);
            if (nodeObj instanceof CCNode && nodeObj instanceof CCTouchDelegate)
            {
                CCTouchDelegate touchDel = (CCTouchDelegate) nodeObj;
                
                switch (touch.getCurrentState())
                {
                    case MultipointTouch.POINTER_PRESSED:
                        touchDel.touchBegan(touch);
                        break;
                    case MultipointTouch.POINTER_DRAGGED:
                        touchDel.touchMoved(touch);
                        break;
                    case MultipointTouch.POINTER_RELEASED:
                        touchDel.touchEnded(touch);
                        break;
                    default:
                        break;
                }
            }
        }
        
        return TBTouch.TOUCH_RESULT_HANDLING;
    }
}
