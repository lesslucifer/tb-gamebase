/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.game.scene;

import java.util.Hashtable;

/**
 *
 * @author KieuAnh
 */
public abstract class CCSceneBank
{
    private final Hashtable scenes = new Hashtable();
    
    public CCScene getScene(String name)
    {
        CCScene scene = (CCScene) this.scenes.get(name);
        
        if (scene == null)
        {
            scene = this.createNewScene(name);
            scene.setName(name);
            this.scenes.put(name, scene);
        }
        
        return scene;
    }
    
    public abstract CCScene createNewScene(String name);
    
    public CCScene removeScene(String name)
    {
        return (CCScene) this.scenes.remove(name);
    }
    
    public void clear()
    {
        this.scenes.clear();
    }
}
