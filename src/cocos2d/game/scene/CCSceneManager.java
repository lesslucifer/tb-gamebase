/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.game.scene;

import cocos2d.game.CCGame;
import cocos2d.touch.CCTouch;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;
import tbgb.entity.GameEntity;
import tbgb.gesturehandle.TBTouchTarget;
import tbgb.gesturehandle.touch.TBTouch;
import tbgb.process.render.RenderProcess;
import tbgb.process.render.RenderProcessManager;
import tbgb.process.update.UpdateGameProcessManager;
import tbgb.process.update.UpdateProcess;

/**
 *
 * @author KieuAnh
 */
public class CCSceneManager implements GameEntity, UpdateProcess, RenderProcess, TBTouchTarget
{
    private final Vector scenes = new Vector();
    private boolean saveScreen = false;
    private final CCGame game;
    
    public CCSceneManager(CCGame game)
    {
        this.game = game;
        
        game.getTouchDispatchers().addTarget(this);
    }
    
    public void update(long eslapedGameTime)
    {
        for (int i = this.scenes.size() - 1; i >= 0; --i)
        {
            Object sceneObj = this.scenes.elementAt(i);
            if (sceneObj instanceof CCScene)
            {
                CCScene scene = (CCScene) sceneObj;
                if (scene.isEnabled())
                {
                    scene.update(eslapedGameTime);
                }
            }
        }
    }
    
    public void render(Graphics g, long eslapedGameTime)
    {
        for (int i = 0; i < this.scenes.size(); ++i)
        {
            Object sceneObj = this.scenes.elementAt(i);
            if (sceneObj instanceof CCScene)
            {
                CCScene scene = (CCScene) sceneObj;
                if (scene.isVisibled())
                {
                    scene.render(g, eslapedGameTime);
                }
            }
        }
    }
    
    public CCScene Current()
    {
        if (this.scenes.size() > 0)
        {
            return (CCScene) this.scenes.elementAt(this.scenes.size() - 1);
        }

        return null;
    }
    
    
    public void AddExclusive(CCScene screen)
    {
        if (this.Current() != null)
        {
            this.Current().setState(CCScene.Deactivated);
        }

        screen.setState(CCScene.Activated);
        this.AddScreen(screen);
    }

    public void AddPopup(CCScene screen)
    {
        if (this.Current() != null)
        {
            this.Current().setState(CCScene.Paused);
        }

        screen.setState(CCScene.Activated);
        this.AddScreen(screen);
    }

    public void AddImmediately(CCScene screen)
    {
        this.AddScreen(screen);
    }

    public void RemoveCurrent()
    {
        this.Current().setState(CCScene.Deactivated);
        this.scenes.removeElementAt(this.scenes.size() - 1);

        if (this.scenes.size() > 0)
        {
            this.Current().setState(CCScene.Activated);
        }
    }

    public void RemoveScreen(CCScene screen)
    {
        for (int i = 0; i < this.scenes.size(); )
        {
            Object sceneObj = this.scenes.elementAt(i);
            if (sceneObj instanceof CCScene)
            {
                CCScene scene = (CCScene) sceneObj;
                if (scene.getName().equals(screen.getName()))
                {
                    this.scenes.removeElementAt(i);
                }
                else
                {
                    ++i;
                }
            }
        }
    }

    private void AddScreen(CCScene screen)
    {
        if (!this.isSaveScreen())
        {
            this.RemoveScreen(screen);
        }

        this.scenes.addElement(screen);
    }

    /**
     * @return the saveScreen
     */
    public boolean isSaveScreen() {
        return saveScreen;
    }

    /**
     * @param SaveScreen the saveScreen to set
     */
    public void setSaveScreen(boolean SaveScreen) {
        this.saveScreen = SaveScreen;
    }

    public int onTouch(TBTouch touch)
    {
        for (int i = 0; i < scenes.size(); i++) {
            Object scObj = scenes.elementAt(i);
            if (scObj instanceof CCScene)
            {
                CCScene scene = (CCScene) scObj;
                if (scene.isEnabled())
                {
                    scene.hasTouch(new CCTouch(touch));
                }
            }
        }
        
        return TBTouch.TOUCH_RESULT_HANDLING;
    }

    public int isHandle(TBTouch touch)
    {
        return TBTouch.TOUCH_RESULT_HANDLING;
    }

    public Object getProcess(int processID)
    {
        if (processID == UpdateGameProcessManager.PROCESS_KEY ||
            processID == RenderProcessManager.PROCESS_KEY)
        {
            return this;
        }
        
        return null;
    }
}
