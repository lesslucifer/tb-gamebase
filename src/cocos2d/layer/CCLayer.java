/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.layer;

import cocos2d.node.CCBaseNode;
import cocos2d.node.CCNode;
import cocos2d.touch.CCTouch;
import cocos2d.touch.CCTouchDelegate;
import java.util.Vector;

/**
 *
 * @author KieuAnh
 */
public class CCLayer extends CCBaseNode implements CCTouchDelegate
{
    public void touchBegan(CCTouch touch)
    {
        Vector childs = this.getAllChildrens();
        for (int i = 0; i < childs.size(); i++) {
            Object childObj = childs.elementAt(i);
            if (childObj instanceof CCNode && childObj instanceof CCTouchDelegate)
            {
                CCNode node = (CCNode) childObj;
                CCTouchDelegate touchDel = (CCTouchDelegate) childObj;
                
                touchDel.touchBegan(touch);
            }
        }
    }

    public void touchMoved(CCTouch touch)
    {
        Vector childs = this.getAllChildrens();
        for (int i = 0; i < childs.size(); i++) {
            Object childObj = childs.elementAt(i);
            if (childObj instanceof CCNode && childObj instanceof CCTouchDelegate)
            {
                CCNode node = (CCNode) childObj;
                CCTouchDelegate touchDel = (CCTouchDelegate) childObj;
                
                touchDel.touchMoved(touch);
            }
        }
    }

    public void touchEnded(CCTouch touch)
    {
        Vector childs = this.getAllChildrens();
        for (int i = 0; i < childs.size(); i++) {
            Object childObj = childs.elementAt(i);
            if (childObj instanceof CCNode && childObj instanceof CCTouchDelegate)
            {
                CCNode node = (CCNode) childObj;
                CCTouchDelegate touchDel = (CCTouchDelegate) childObj;
                
                touchDel.touchEnded(touch);
            }
        }
    }
    
}
