/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.common;

import tbgb.components.twodimesion.point2dimplements.Point2DSize;

/**
 *
 * @author KieuAnh
 */
public class CCSize extends Point2DSize
{
    public CCSize()
    {
    }
    
    public CCSize(float w, float h)
    {
        this.X = w;
        this.Y = h;
    }
    
    public CCSize(CCSize p)
    {
        this.X = p.X;
        this.Y = p.Y;
    }
    
    public CCSize copy()
    {
        return new CCSize(this);
    }
}
