/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.common;

import tbgb.components.twodimesion.point2dimplements.Point2D;

/**
 *
 * @author KieuAnh
 */
public class CCVector extends CCPoint
{
    public static final CCVector ZERO = new CCVector();
    
    public CCVector()
    {
        super();
    }
    
    public CCVector(float x, float y)
    {
        super(x, y);
    }
    
    public CCVector(Point2D p)
    {
        super(p);
    }
    
    public void normalize()
    {
        float len = this.length();
        
        if (len != 0.0)
        {
            this.X /= len;
            this.Y /= len;
        }
    }
    
    public float length()
    {
        return (float) Math.sqrt(this.getSquaredLength());
    }
    
    public CCVector mult(float x)
    {
        CCVector prod = new CCVector(this);
        prod.saMult(x);
        return prod;
    }
}
