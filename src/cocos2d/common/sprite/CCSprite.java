/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.common.sprite;

import cocos2d.common.CCRect;
import cocos2d.node.CCNode;

/**
 *
 * @author KieuAnh
 */
public interface CCSprite extends CCNode
{
    /**
     * @return the data
     */
    CCSpriteData getData();

    /**
     * @return the delay
     */
    long getDelay();

    float getFPS();

    /**
     * @return the transform
     */
    int getTransform();

    /**
     * @return the animate
     */
    boolean isAnimate();

    /**
     * @param animate the animate to set
     */
    void setAnimate(boolean animate);

    /**
     * @return the currentFrameIndex
     */
    public int getCurrentFrameIndex();

    /**
     * @param currentFrameIndex the currentFrameIndex to set
     */
    void setCurrentFrameIndex(int currentFrameIndex);

    void setFPS(float fps);

    /**
     * @param transform the transform to set
     */
    void setTransform(int transform);
    

    /**
     * @return the align
     */
    public int getAlign();

    /**
     * @param align the align to set
     * Use Graphics.TOP, Graphics.LEFT, Graphics.RIGHT, Graphics.BOTTOM
     */
    public void setAlign(int align);
}
