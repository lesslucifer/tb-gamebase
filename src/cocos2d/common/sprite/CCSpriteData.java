/*
 * To change sprData license header, choose License Headers in Project Properties.
 * To change sprData template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.common.sprite;

import cocos2d.common.CCException;
import cocos2d.common.CCPoint;
import cocos2d.common.factory.CCFactory;
import cocos2d.common.sprite.frame.CCImageFrame;
import tbgb.resourcemanager.TBResourceManager;
import tbgb.serialization.json.JSONArray;
import tbgb.serialization.json.JSONException;
import tbgb.serialization.json.JSONObject;

/**
 *
 * @author KieuAnh
 */
public class CCSpriteData
{
    private String spriteName;
    private CCImageFrame[] frames;
    private final CCPoint anchor = new CCPoint();

    public CCSpriteData(String spriteName, CCImageFrame[] frames)
    {
        this.spriteName = spriteName;
        this.frames = frames;
    }
    
    private CCSpriteData()
    {
        super();
    }
    
    public static CCSpriteData deser(String name, JSONObject jsonSD) throws JSONException, CCException
    {
        TBResourceManager rm = CCFactory.inst();
        
        CCSpriteData sprData = new CCSpriteData();
        sprData.spriteName = name;
        
        JSONArray frames = (JSONArray) jsonSD.getJSONArray("frames");
        sprData.frames = new CCImageFrame[frames.length()];
        for (int i = 0; i < frames.length(); ++i)
        {
            String frameName = frames.getString(i);
            CCImageFrame frame = (CCImageFrame) rm.getResource(CCImageFrame.class, frameName);
            if (frame == null)
            {
                throw new CCException("Frame: " + frameName + " not found");
            }
            else
            {
                sprData.frames[i] = frame;
            }
        }

        if (jsonSD.has("anchorX") && jsonSD.has("anchorY"))
        {
            sprData.anchor().setX(jsonSD.getInt("anchorX"));
            sprData.anchor().setY(jsonSD.getInt("anchorY"));
        }
        
        return sprData;
    }

    /**
     * @return the spriteName
     */
    public String getSpriteName() {
        return spriteName;
    }

    /**
     * @return the frames
     */
    public CCImageFrame[] getFrames() {
        return frames;
    }
    
    public CCPoint anchor()
    {
        return this.anchor;
    }

    public String toString() {
        return "CCSpriteData{" + "spriteName=" + spriteName + ", frames=" + frames + ", anchor=" + anchor + '}';
    }
}