/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.common.sprite.frame;

import cocos2d.common.CCPoint;
import cocos2d.common.CCRect;
import javax.microedition.lcdui.Image;
import tbgb.serialization.json.JSONException;
import tbgb.serialization.json.JSONObject;

/**
 * A frame (CCRect) of an image
 * @author KieuAnh
 */
public class CCImageFrame
{
    private final Image image;
    private final CCRect frame;

    public CCImageFrame(Image image, CCRect frame) {
        this.image = image;
        this.frame = frame;
    }

    /**
     * @return the image
     */
    public Image getImage() {
        return image;
    }

    /**
     * @return the frame
     */
    public CCRect getFrame() {
        return frame;
    }
    
    public static CCImageFrame deser(Image img, JSONObject json) throws JSONException
    {
        CCRect frame = CCRect.deser(json);
        CCImageFrame imgFrame = new CCImageFrame(img, frame);
        
        return imgFrame;
    }
}