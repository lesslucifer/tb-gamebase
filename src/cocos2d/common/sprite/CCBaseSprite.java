/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.common.sprite;

import cocos2d.common.CCPoint;
import cocos2d.common.sprite.frame.CCImageFrame;
import cocos2d.layer.CCLayer;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.game.Sprite;
import tbgb.BaseGame;
import tbgb.log.TBLog;

/**
 *
 * @author KieuAnh
 */
public final class CCBaseSprite extends CCLayer implements CCSprite
{
    private final CCSpriteData data;
    private CCImageFrame currentFrame;
    private int currentFrameIndex;
    private boolean animate = true;
    private long delay = 0, cTime = 0;
    private int transform = Sprite.TRANS_NONE;
    private int align = Graphics.HCENTER | Graphics.VCENTER;
    
    public CCBaseSprite(CCSpriteData data)
    {
        super();
        
        this.data = data;
        this.delay = this.gameLoopInterval();
        this.setCurrentFrameIndex(0);
        
        this.getAnchor().set(data.anchor());
        
        this.getBound().getSize().set(this.currentFrame.getFrame().getSize());
    }
    
    public void draw(Graphics g, long dt)
    {
        if (this.isVisibled())
        {
            if (isAnimate() && getData().getFrames().length > 1)
            {
                this.cTime -= dt;
                if (this.cTime <= 0)
                {
                    this.setCurrentFrameIndex(this.getCurrentFrameIndex() + 1);
                }
            }
            
            if (!this.currentFrame.getFrame().isEmpty())
            {
                CCPoint globalPos = this.getGlobalPosition();
                
                try
                {
                    g.drawRegion(getCurrentFrame().getImage(),
                            (int) getCurrentFrame().getFrame().getPosition().X,
                            (int) getCurrentFrame().getFrame().getPosition().Y,
                            (int) getCurrentFrame().getFrame().getSize().X,
                            (int) getCurrentFrame().getFrame().getSize().Y,
                            getTransform(),
                            (int) globalPos.X,
                            (int) globalPos.Y,
                            this.align);
                }
                catch (Exception e)
                {
                    TBLog.logError(e.toString());
                }
            }
            
            super.draw(g, dt);
        }
    }
    
    public float getFPS()
    {
        return (float) (1000 * this.getDelay()) / this.gameLoopInterval();
    }
    
    public void setFPS(float fps)
    {
        this.delay = (long) ((fps * this.gameLoopInterval()) / 1000f);
        this.cTime = this.getDelay();
    }
    
    private long gameLoopInterval()
    {
        return BaseGame.DEFAULT_LOOP_INTERVAL;
    }

    /**
     * @return the data
     */
    public CCSpriteData getData() {
        return data;
    }

    /**
     * @return the currentFrame
     */
    public CCImageFrame getCurrentFrame() {
        return currentFrame;
    }

    /**
     * @return the currentFrameIndex
     */

    public int getCurrentFrameIndex() {
        return currentFrameIndex;
    }

    /**
     * @param currentFrameIndex the currentFrameIndex to set
     */
    public void setCurrentFrameIndex(int currentFrameIndex) {
        this.currentFrameIndex = currentFrameIndex;
        
        if (this.getCurrentFrameIndex() >= this.getData().getFrames().length)
        {
            this.currentFrameIndex = 0;
        }

        this.currentFrame = this.getData().getFrames()[this.currentFrameIndex];
        this.cTime = this.getDelay();
    }

    /**
     * @return the animate
     */
    public boolean isAnimate() {
        return animate;
    }

    /**
     * @param animate the animate to set
     */
    public void setAnimate(boolean animate) {
        this.animate = animate;
    }

    /**
     * @return the delay
     */
    public long getDelay() {
        return delay;
    }

    /**
     * @return the transform
     */
    public int getTransform() {
        return transform;
    }

    /**
     * @param transform the transform to set
     */
    public void setTransform(int transform) {
        this.transform = transform;
    }

    public String toString() {
        return "CCBaseSprite{" + "data=" + data + ", currentFrame=" + currentFrame + ", currentFrameIndex=" + currentFrameIndex + ", animate=" + animate + ", delay=" + delay + ", cTime=" + cTime + ", transform=" + transform + '}';
    }

    /**
     * @return the align
     */
    public int getAlign() {
        return align;
    }

    /**
     * @param align the align to set
     */
    public void setAlign(int align) {
        this.align = align;
    }
}
