/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.common;

import tbgb.components.twodimesion.BoundComponent;
import tbgb.serialization.json.JSONException;
import tbgb.serialization.json.JSONObject;

/**
 *
 * @author KieuAnh
 */
public class CCRect
{
    public static final CCRect EMPTY = new CCRect();
    
    private final CCPoint position;
    private final CCSize size;
    
    public CCRect()
    {
        this.position = new CCPoint();
        this.size = new CCSize();
    }
    
    public CCRect(float x, float y, float w, float h)
    {
        this.position = new CCPoint(x, y);
        this.size = new CCSize(w, h);
    }
    
    public CCRect(BoundComponent bound)
    {
        this.position = new CCPoint(bound.getPositionComponent().getX(), bound.getPositionComponent().getY());
        this.size = new CCSize(bound.getSizeComponent().getWidth(), bound.getSizeComponent().getHeight());
    }
    
    public CCRect(CCPoint p, CCSize s)
    {
        this.position = p.copy();
        this.size = s.copy();
    }
    
    public CCRect(CCRect other)
    {
        this.position = other.position.copy();
        this.size = other.size.copy();
    }

    /**
     * @return the position
     */
    public CCPoint getPosition() {
        return position;
    }

    /**
     * @return the size
     */
    public CCSize getSize() {
        return size;
    }
    
    public CCRect copy()
    {
        return new CCRect(this);
    }
    
    public void toEmpty()
    {
        this.position.toZero();
        this.size.toZero();
    }
    
    public boolean isEmpty()
    {
        return this.size.getSquaredLength() <= 0;
    }
    
    public String toString()
    {
        return "{org: " + this.position + "; size: " + this.size + "}";
    }

    public CCPoint getCenter()
    {
        return this.getPosition().add(new CCPoint(this.getSize().X / 2f, this.getSize().Y / 2f));
    }
    
    public static CCRect deser(JSONObject jsonObject) throws JSONException
    {
        CCRect r = new CCRect();
        
        r.getPosition().set(jsonObject.getInt("x"), jsonObject.getInt("y"));
        r.getSize().set(jsonObject.getInt("w"), jsonObject.getInt("h"));
        
        return r;
    }

    public boolean intersect(CCRect r)
    {
        CCRect r1 = this, r2 = r;
        
        return !(r2.position.X > r1.position.X + r1.size.X || 
           r2.position.X + r2.size.X < r1.position.X || 
           r2.position.Y > r1.position.Y + r1.size.Y ||
           r2.position.Y + r2.size.Y < r1.position.Y);
    }
    
    
    public float getX()
    {
        return position.getX();
    }
    
    public float getY()
    {
        return position.getY();
    }
    
    public void setX(float x)
    {
        this.position.setX(x);
    }
    
    public void setY(float y)
    {
        this.position.setY(y);
    }
    
    public void add(float dx, float dy)
    {
        this.position.saAdd(dx, dy);
    }
    
    public float getWidth()
    {
        return this.size.getWidth();
    }
    
    public float getHeight()
    {
        return this.size.getHeight();
    }
    
    public void setWidth(float width)
    {
        this.size.setWidth(width);
    }
    
    public void setHeight(float height)
    {
        this.size.setHeight(height);
    }
    
    public float getLeft()
    {
        return this.position.getX();
    }
    
    public float getRight()
    {
        return this.position.getX() + this.size.getWidth();
    }
    
    public float getTop()
    {
        return this.position.getY();
    }
    
    public float getBottom()
    {
        return this.position.getY() + this.size.getHeight();
    }
    
    public void setLeft(float left)
    {
        this.position.setX(left);
    }
    
    public void setRight(float right)
    {
        this.size.setWidth(right - this.position.getX());
    }
    
    public void setTop(float top)
    {
        this.position.setY(top);
    }
    
    public void setBottom(float bottom)
    {
        this.size.setHeight(bottom - this.position.getY());
    }

    public float getSquaredLength() {
        return this.size.getSquaredLength();
    }

    public float getCenterX() {
        return (this.getRight()- this.getLeft()) / 2;
    }

    public float getCenterY() {
        return (this.getBottom() - this.getTop()) / 2;
    }

    public void setCenterX(float cX) {
        this.setLeft(cX + this.getWidth() / 2);
    }

    public void setCenterY(float cY) {
        this.setTop(cY + this.getHeight()/ 2);
    }
}
