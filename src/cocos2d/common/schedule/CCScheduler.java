/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.common.schedule;

/**
 *
 * @author KieuAnh
 */
public interface CCScheduler {
    void schedule(CCSchedule schedule);
    void schedule(CCSchedule schedule, long time);
    void schedule(CCSchedule schedule, long time, int priority);
    void unschedule(CCSchedule schedule);
    
    void run(long dt);
}