/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.common.schedule;

import java.util.Vector;
import tbgb.log.TBLog;

/**
 *
 * @author KieuAnh
 */
public class CCBaseScheduler implements CCScheduler
{
    private static class CCSchedulepProxy
    {
        private long expected;
        private long remain;
        private int priority;
        private final CCSchedule schedule;

        public CCSchedulepProxy(CCSchedule sch, long expected, int prio) {
            this.schedule = sch;
            this.expected = expected;
            this.remain = 0;
            this.priority = prio;
        }

        public long getExpected() {
            return expected;
        }

        public void setExpected(long expected) {
            this.expected = expected;
        }

        public long getRemain() {
            return remain;
        }

        public void setRemain(long remain) {
            this.remain = remain;
        }

        /**
         * @return the priority
         */
        public int getPriority() {
            return priority;
        }

        /**
         * @param priority the priority to set
         */
        public void setPriority(int priority) {
            this.priority = priority;
        }

        /**
         * @return the schedule
         */
        public CCSchedule getSchedule() {
            return schedule;
        }
    }
    
    private final Vector schedules = new Vector();
    
    public void schedule(CCSchedule schedule)
    {
        this.schedule(schedule, 0, 0);
    }

    public void schedule(CCSchedule schedule, long time)
    {
        this.schedule(schedule, time, 0);
    }
    
    public void schedule(CCSchedule schedule, long time, int priority)
    {
        TBLog.log("Schedule " + schedule);
        CCSchedulepProxy schProxy = this.getSchedluleProxy(schedule);
        if (schProxy != null)
        {
            TBLog.log("Found re-schedule: " + schedule);
            // update time
            schProxy.setExpected(time);
            
            // re-add schedule if priority is changed
            if (priority != schProxy.priority)
            {
                TBLog.log("Update priority");
                this.schedules.removeElement(schProxy);
                schProxy.setPriority(priority);
                this.addScheduleProxy(schProxy);
            }
        }
        else
        {
            schProxy = new CCSchedulepProxy(schedule, time, priority);
            this.addScheduleProxy(schProxy);
        }
    }

    public void unschedule(CCSchedule schedule)
    {
        int removedIndex = this.getSchedluleProxyIndex(schedule);
        if (removedIndex > 0)
        {
            this.schedules.removeElementAt(removedIndex);
        }
    }
    
    private void addScheduleProxy(CCSchedulepProxy schProxy)
    {
        for (int i = 0; i < schedules.size(); i++) {
            CCSchedulepProxy otherProxy = (CCSchedulepProxy) schedules.elementAt(i);
            if (otherProxy.getPriority() < schProxy.getPriority())
            {
                this.schedules.insertElementAt(schProxy, i);
                return;
            }
        }
        
        this.schedules.addElement(schProxy);
    }
    
    private CCSchedulepProxy getSchedluleProxy(CCSchedule sch)
    {
        int i = this.getSchedluleProxyIndex(sch);
        if (i >= 0)
        {
            return (CCSchedulepProxy) this.schedules.elementAt(i);
        }
        
        return null;
    }
    
    
    private int getSchedluleProxyIndex(CCSchedule sch)
    {
        for (int i = 0; i < schedules.size(); i++) {
            CCSchedulepProxy ccShProxy = (CCSchedulepProxy) schedules.elementAt(i);
            if (ccShProxy.getSchedule() == sch)
            {
                return i;
            }
        }
        
        return -1;
    }

    public void run(long dt)
    {
        for (int i = 0; i < this.schedules.size(); i++) {
            CCSchedulepProxy schProxy = (CCSchedulepProxy) this.schedules.elementAt(i);
            schProxy.remain += dt;
            
            if (schProxy.remain >= schProxy.expected)
            {
                schProxy.getSchedule().run(schProxy.remain);
                
                schProxy.remain = 0;
            }
        }
    }
}
