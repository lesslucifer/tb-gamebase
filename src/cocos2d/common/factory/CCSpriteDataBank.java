/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.common.factory;

import cocos2d.common.CCException;
import cocos2d.common.sprite.CCSpriteData;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import tbgb.resourcemanager.TBResourceLoader;
import tbgb.serialization.json.JSONException;
import tbgb.serialization.json.JSONObject;

/**
 *
 * @author KieuAnh
 */
public class CCSpriteDataBank implements TBResourceLoader
{
    private final Hashtable spriteDatas = new Hashtable();
    
    private CCSpriteDataBank()
    {
        super();
    }
    
    private static final CCSpriteDataBank inst = new CCSpriteDataBank();
    
    public static CCSpriteDataBank inst()
    {
        return inst;
    }
    
    public Class getResourceKey() {
        return CCSpriteData.class;
    }

    public boolean isReuseResource()
    {
        return false;
    }

    public Object loadResource(String resourceName)
    {
        return this.spriteDatas.get(resourceName);
    }
    
    public void addSpriteData(CCSpriteData sprData)
    {
        this.spriteDatas.put(sprData.getSpriteName(), sprData);
    }
    
    public void addSpriteDataFromJSON(String jsonFile) throws IOException, JSONException, CCException
    {
        InputStream is = getClass().getResourceAsStream(jsonFile);
        byte[] sBuffer = new byte[is.available()];
        is.read(sBuffer);
        String jsonData = new String(sBuffer);

        JSONObject jsonSDs = new JSONObject(jsonData);
        Enumeration jsonSDKeys = jsonSDs.keys();
        while (jsonSDKeys.hasMoreElements())
        {
            String jsonSDName = (String) jsonSDKeys.nextElement();
            JSONObject jsonSD = jsonSDs.getJSONObject(jsonSDName);

            CCSpriteData ccSD = CCSpriteData.deser(jsonSDName, jsonSD);
            this.addSpriteData(ccSD);
        }
    }
}
