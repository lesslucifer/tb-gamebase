/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.common.factory;

import cocos2d.common.CCException;
import cocos2d.common.sprite.frame.CCImageFrame;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.microedition.lcdui.Image;
import tbgb.log.TBLog;
import tbgb.resourcemanager.TBResourceLoader;
import tbgb.resourcemanager.TBResourceManager;
import tbgb.serialization.json.JSONException;
import tbgb.serialization.json.JSONObject;

/**
 *
 * @author KieuAnh
 */
public class CCFrameImageBank implements TBResourceLoader
{
    private final Hashtable frames = new Hashtable();
    
    private CCFrameImageBank()
    {
        super();
    }
    
    private static final CCFrameImageBank inst = new CCFrameImageBank();
    
    public static CCFrameImageBank inst()
    {
        return inst;
    }
    
    public Class getResourceKey()
    {
        return CCImageFrame.class;
    }

    public boolean isReuseResource()
    {
        return false;
    }

    public Object loadResource(String resourceName)
    {
        return this.frames.get(resourceName);
    }
    
    public void addFrame(String frName, CCImageFrame frame)
    {
        this.frames.put(frName, frame);
    }
    
    public void loadFromJSONFile(String jsonFile) throws IOException, JSONException, CCException
    {
        final TBResourceManager rm = CCFactory.inst();

        InputStream is = getClass().getResourceAsStream(jsonFile);
        byte[] sBuffer = new byte[is.available()];
        is.read(sBuffer);
        String jsonData = new String(sBuffer);

        JSONObject jsonFD = new JSONObject(jsonData);
        String imgName = jsonFD.getString("image");
        Image image = (Image) rm.getResource(Image.class, imgName);
        if (image == null)
        {
            throw new CCException(this.getClass().getName() +
                    " loadJSON: " + jsonFile +
                    " image: " + imgName + " empty");
        }

        JSONObject jsonFrames = jsonFD.getJSONObject("frames");
        Enumeration jsonFramesKeys = jsonFrames.keys();
        while (jsonFramesKeys.hasMoreElements())
        {
            String jsonFrameName = (String) jsonFramesKeys.nextElement();
            JSONObject jsonFrame = jsonFrames.getJSONObject(jsonFrameName);

            try
            {
                CCImageFrame imgFrame = CCImageFrame.deser(image, jsonFrame);
                this.addFrame(jsonFrameName, imgFrame);
            }
            catch (JSONException jex)
            {
                TBLog.logError("JSON parse error: " + jex);
            }
        }
    }
}
