/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.common.factory;

import cocos2d.common.CCRect;
import cocos2d.common.sprite.CCBaseSprite;
import cocos2d.common.sprite.CCSprite;
import cocos2d.common.sprite.CCSpriteData;
import cocos2d.common.sprite.frame.CCImageFrame;
import javax.microedition.lcdui.Image;
import tbgb.resourcemanager.TBResourceLoader;
import tbgb.resourcemanager.TBResourceManager;

/**
 *
 * @author KieuAnh
 */
public class CCSpriteLoader implements TBResourceLoader
{
    private final TBResourceManager rm;
    
    public CCSpriteLoader(TBResourceManager resMan)
    {
        this.rm = resMan;
    }

    public Class getResourceKey()
    {
        return CCSprite.class;
    }

    public boolean isReuseResource()
    {
        return false;
    }

    public Object loadResource(String resourceName)
    {
        CCSpriteData spriteData = (CCSpriteData)
                this.rm.getResource(CCSpriteData.class, resourceName);
        
        if (spriteData == null)
        {
            // try to make ccsprite from framed image
            CCImageFrame framedImg = (CCImageFrame) this.rm.getResource(CCImageFrame.class, resourceName);
            if (framedImg == null)
            {
                // try to make framed image from image
                Image image = (Image) this.rm.getResource(Image.class, resourceName);
                if (image != null)
                {
                    framedImg = new CCImageFrame(image, new CCRect(0, 0, image.getWidth(), image.getHeight()));
                }
            }
            
            if (framedImg != null)
            {
                spriteData = new CCSpriteData(resourceName,
                        new CCImageFrame[] {framedImg});
            }
        }
        
        if (spriteData != null)
        {
            return new CCBaseSprite(spriteData);
        }
        
        return null;
    }
}
