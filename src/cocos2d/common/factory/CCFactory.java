/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.common.factory;

import cocos2d.common.sprite.CCSprite;
import tbgb.resourcemanager.ImageResourceLoader;
import tbgb.resourcemanager.TBBaseResourceManager;


/**
 *
 * @author KieuAnh
 */
public class CCFactory extends TBBaseResourceManager
{
    private CCFactory()
    {
        super();
        
        this.addResourceLoader(new ImageResourceLoader());
        this.addResourceLoader(CCFrameImageBank.inst());
        this.addResourceLoader(CCSpriteDataBank.inst());
        this.addResourceLoader(new CCSpriteLoader(this));
    }
    
    private static final CCFactory inst = new CCFactory();
    
    public static CCFactory inst()
    {
        return inst;
    }
    
    public CCSprite createSprite(String spriteName)
    {
        Object sprObj = this.getResource(CCSprite.class, spriteName);
        if (sprObj instanceof CCSprite)
        {
            return (CCSprite) sprObj;
        }
        
        return null;
    }
}
