/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cocos2d.common;

import tbgb.components.twodimesion.point2dimplements.Point2D;
import tbgb.components.twodimesion.point2dimplements.Point2DPosition;

/**
 *
 * @author KieuAnh
 */
public class CCPoint extends Point2DPosition
{
    public CCPoint()
    {
        
    }
    
    public CCPoint(float x, float y)
    {
        this.X = x;
        this.Y = y;
    }
    
    public CCPoint(CCPoint p)
    {
        this.X = p.X;
        this.Y = p.Y;
    }
    
    public CCPoint(Point2D p)
    {
        super();
        this.X = p.X;
        this.Y = p.Y;
    }
    
    public CCPoint copy()
    {
        return new CCPoint(this);
    }
    
    public CCPoint add(CCPoint other)
    {
        CCPoint p = this.copy();
        p.saAdd(other);
        
        return p;
    }
    
    public void saAdd(CCPoint other)
    {
        this.saAdd(other.X, other.Y);
    }
    
    public CCPoint sub(CCPoint other)
    {
        CCPoint p = this.copy();
        p.saSub(other);
        
        return p;
    }

    public void saSub(CCPoint other)
    {
        this.X -= other.X;
        this.Y -= other.Y;
    }
    
    /**
     * self-assignment multiply a point with
     * a multiplied integer
     * @param n
     * @param m
     */
    public void saMMult(int n, int m)
    {
        this.X *= n;
        this.X /= m;
        this.Y *= n;
        this.Y /= m;
    }
    
    public CCPoint mMult(int n, int m)
    {
        CCPoint res = new CCPoint(this);
        res.saMMult(n, m);
        
        return res;
    }
    
    public CCPoint negative()
    {
        return new CCPoint(-X, -Y);
    }
}