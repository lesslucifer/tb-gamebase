/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.gesturehandle.touch;

import com.nokia.mid.ui.multipointtouch.MultipointTouch;
import com.nokia.mid.ui.multipointtouch.MultipointTouchListener;
import java.util.Hashtable;
import java.util.Vector;
import tbgb.gesturehandle.TBTouchDispatcher;
import tbgb.log.TBLog;
import tbgb.utils.collection.TracedHashtable;

/**
 *
 * @author LưuQuang
 */
public class TBTouchController implements MultipointTouchListener
{
    public static final int MAX_TOUCHES = MultipointTouch.getMaxPointers();
    
    private final TracedHashtable touches = new TracedHashtable();
    private final Vector dispatchers = new Vector();
    
    public void pointersChanged(int[] touchIDs)
    {
        this.touches.beginTrace();
        for (int i = 0; i < touchIDs.length; ++i)
        {
            Integer touchID = new Integer(touchIDs[i]);
            if (this.touches.containsKey(touchID))
            {
                Object touchObject = this.touches.getAndMarkTraced(touchID);
                if (touchObject instanceof TBTouch)
                {
                    TBTouch touch = (TBTouch) touchObject;
                    touch.updateLocation();
                }
            }
            else
            {
                this.touches.put(touchID, new TBTouch(touchID.intValue()));
            }
        }

        this.touches.endTrace();
        
        this.dispatchTouches();
    }
    
    public Hashtable getTouches()
    {
        return this.touches;
    }

    private void dispatchTouches()
    {
        for (int i = 0; i < this.getDispatchers().size(); i++) {
            Object distpObj = this.getDispatchers().elementAt(i);
            if (distpObj instanceof TBTouchDispatcher)
            {
                TBTouchDispatcher distp = (TBTouchDispatcher) distpObj;
                distp.dispatch(this);
            }
        }
    }

    /**
     * @return the dispatchers
     */
    public Vector getDispatchers() {
        return dispatchers;
    }
}
