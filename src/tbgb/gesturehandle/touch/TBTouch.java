/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.gesturehandle.touch;

import com.nokia.mid.ui.multipointtouch.MultipointTouch;
import tbgb.components.twodimesion.point2dimplements.Point2D;

/**
 *
 * @author LưuQuang
 */
public class TBTouch
{
    public static final int TOUCH_RESULT_EXCLUSIVED = 2;
    public static final int TOUCH_RESULT_HANDLING = 1;
    public static final int TOUCH_RESULT_NONE = 0;
    
    private final int touchID;
    private final Point2D current, last, begin;
    private int lastState, currentState;
    
    public TBTouch(int id)
    {
        this.touchID = id;
        this.current = new Point2D(MultipointTouch.getX(touchID), MultipointTouch.getY(touchID));
        this.last = new Point2D(Integer.MIN_VALUE, Integer.MIN_VALUE);
        this.begin = new Point2D(this.current.X, this.current.Y);
        this.lastState = MultipointTouch.POINTER_PRESSED;
        this.currentState = MultipointTouch.POINTER_PRESSED;
    }
    
    final void updateLocation()
    {
        this.last.set(this.current);
        this.lastState = this.getCurrentState();
        
        this.current.X = MultipointTouch.getX(touchID);
        this.current.Y = MultipointTouch.getY(touchID);
        this.currentState = MultipointTouch.getState(touchID);
    }

    /**
     * @return the touchID
     */
    public int getTouchID() {
        return touchID;
    }

    /**
     * @return the current
     */
    public Point2D getCurrent() {
        return current;
    }

    /**
     * @return the last
     */
    public Point2D getLast() {
        return last;
    }

    /**
     * @return the begin
     */
    public Point2D getBegin() {
        return begin;
    }

    /**
     * @return the state
     */
    public int getState() {
        return MultipointTouch.getState(touchID);
    }

    /**
     * @return the lastState
     */
    public int getLastState() {
        return lastState;
    }

    /**
     * @return the currentState
     */
    public int getCurrentState() {
        return currentState;
    }

    public String toString() {
        return "TBTouch{" + "touchID=" + touchID + ", current=" + current + ", last=" + last + ", lastState=" + lastState + ", currentState=" + currentState + '}';
    }
}
