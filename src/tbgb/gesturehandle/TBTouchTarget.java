/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tbgb.gesturehandle;

import tbgb.gesturehandle.touch.TBTouch;

/**
 *
 * @author KieuAnh
 */
public interface TBTouchTarget
{
    int onTouch(TBTouch touch);
    int isHandle(TBTouch touch);
}
