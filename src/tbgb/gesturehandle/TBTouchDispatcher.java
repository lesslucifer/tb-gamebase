/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tbgb.gesturehandle;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import tbgb.gesturehandle.touch.TBTouch;
import tbgb.gesturehandle.touch.TBTouchController;
import tbgb.log.TBLog;
import tbgb.utils.collection.TracedHashtable;

/**
 *
 * @author KieuAnh
 */
public class TBTouchDispatcher
{
    private final Vector targets = new Vector();
    private final TracedHashtable handledTouches = new TracedHashtable(TBTouchController.MAX_TOUCHES);
  
    public void dispatch(TBTouchController tController)
    {
        this.handledTouches.beginTrace();
        Hashtable touches = tController.getTouches();
        Enumeration touchIDs = touches.keys();
        while (touchIDs.hasMoreElements())
        {
            Integer touchID = (Integer) touchIDs.nextElement();
            TBTouch touch = (TBTouch) touches.get(touchID);
            
            if (touch != null)
            {
                TBTouchTarget targetHandled = (TBTouchTarget) this.handledTouches.getAndMarkTraced(touch);
                if (targetHandled != null)
                {
                    int hResult = targetHandled.onTouch(touch);
                    if (hResult != TBTouch.TOUCH_RESULT_EXCLUSIVED)
                    {
                        this.handledTouches.unmark(targetHandled);
                    }
                }
                else
                {
                    for (int i = 0; i < this.getTargets().size(); ++i)
                    {
                        TBTouchTarget target = (TBTouchTarget) this.getTargets().elementAt(i);
                        int hResult = target.isHandle(touch);
                        
                        if (hResult == TBTouch.TOUCH_RESULT_EXCLUSIVED)
                        {
                                this.handledTouches.put(touch, target);
                                target.onTouch(touch);
                                break;
                        }
                        else if (hResult == TBTouch.TOUCH_RESULT_HANDLING)
                        {
                            target.onTouch(touch);
                        }
                    }
                }
            }
        }
        this.handledTouches.endTrace();
    }

    public boolean addTarget(TBTouchTarget target)
    {
        if (!this.targets.contains(target))
        {
            this.getTargets().addElement(target);
            return true;
        }
        
        return false;
    }
    
    public boolean removeTarget(TBTouchTarget target)
    {
        return this.getTargets().removeElement(target);
    }

    /**
     * @return the targets
     */
    public Vector getTargets() {
        return targets;
    }
}
