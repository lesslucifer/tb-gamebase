/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tbgb.shading;

import javax.microedition.lcdui.Image;

/**
 *
 * @author KieuAnh
 */
public class Shader
{
    private final ColorBuffer backBuffer;
    private final ColorBuffer frontBuffer;
    
    public Shader(int w, int h)
    {
        backBuffer = new ColorBuffer(w, h);
        frontBuffer = new ColorBuffer(w, h);
    }
    
    void renderImage(Image img)
    {
        renderImage(img, 0, 0, img.getWidth(), img.getHeight());
    }
    
    void renderImage(Image img, int x, int y, int w, int h)
    {
        ColorBuffer buffer = this.getFreeBuffer();
        img.getRGB(buffer.raw, 0, buffer.width, x, y, w, h);
    }
    
    ColorBuffer getFreeBuffer()
    {
        return this.backBuffer;
    }
    
    void swapToFront(ColorBuffer back, int x, int y, int w, int h, int toX, int toY)
    {
        // copy line by line
        for (int i = 0; i < h; ++i)
        {
            for (int j = 0; j < w; ++j)
            {
                frontBuffer.raw[toX + j + (toY + i) * frontBuffer.width] =
                        back.raw[(j + x) + (i + y) * back.width];
            }
        }
    }
    
    public static interface Transformer
    {
        
    }
    
    void renderToFront(ColorBuffer buffer, int srcX, int srcY, int srcW, int srcH,
            int anchorX, int anchorY, Transformer transform, int toX, int toY)
    {
    }
}
