/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tbgb.shading;

/**
 *
 * @author KieuAnh
 */
public class Color
{
    public static int color(byte a, byte r, byte g, byte b)
    {
        return (a<<24) | (r<<16) | (g<<8) | (b);
    }
    
    public static int color(byte r, byte g, byte b)
    {
        return (0xff<<24) | (r<<16) | (g<<8) | b;
    }
    
    public static int color(float a, float r, float g, float b)
    {
        return color((byte) (a * 0xff), (byte) (r * 0xff),(byte) (g * 0xff),(byte) (b * 0xff));
    }
    
    public static int color(float r, float g, float b)
    {
        return color((byte) (r * 0xff),(byte) (g * 0xff),(byte) (b * 0xff));
    }
    
    public static int WHITE = color(1f, 1f, 1f);
    public static int BLACK = color(0f, 0f, 0f);
    public static int RED = color(1f, 0f, 0f);
    public static int GREEN = color(0f, 1f, 0f);
    public static int BLUE = color(0f, 0f, 1f);
}
