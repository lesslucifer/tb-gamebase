/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tbgb.shading;

import javax.microedition.lcdui.Graphics;

/**
 *
 * @author KieuAnh
 */
public class ColorBuffer
{
    public int[] raw;
    public final int width, height;
    
    public ColorBuffer(int width, int height)
    {
        this.raw = new int[width * height];
        this.width = width;
        this.height = height;
    }
    
    public int[] getRaw()
    {
        return raw;
    }
    
    public int colorAtIndex(int row, int col)
    {
        return raw[row * width + col];
    }
    
    public void draw(Graphics g)
    {
        g.drawRGB(raw, 0, width, 0, 0, width, height, true);
    }
}
