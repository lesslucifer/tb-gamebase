/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.entity;

import java.util.Hashtable;

/**
 *
 * @author LưuQuang
 */
public class DefaultEntity implements GameEntity
{
    private final Hashtable processes = new Hashtable();
    
    public Object getProcess(int processID) {
        return this.processes.get(new Integer(processID));
    }
    
    public void addProcess(int processID, Object process)
    {
        this.processes.put(new Integer(processID), process);
    }
}
