/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.entity.component;

import java.util.Hashtable;
import tbgb.entity.DefaultEntity;

/**
 *
 * @author LưuQuang
 */
public class ComponentEntity extends DefaultEntity implements GameEntityComponentContainer
{
    private Hashtable components = new Hashtable();

    public Object getComponent(int componentID) {
        return this.components.get(new Integer(componentID));
    }
    
    public void addComponent(int componentID, Object component)
    {
        this.components.put(new Integer(componentID), component);
    }
}
