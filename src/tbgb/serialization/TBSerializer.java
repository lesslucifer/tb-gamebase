/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.serialization;

/**
 *
 * @author LưuQuang
 */
public interface TBSerializer {
    void serializeString(String name, Object obj);
    void serializeObject(String name, TBSerializable serObject);

    TBSerializer subSerializer(String name);
    
}
