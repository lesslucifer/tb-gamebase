/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.serialization.bml;

import java.util.Vector;
import tbgb.serialization.TBDeserializer;
import tbgb.serialization.TBSerializable;
import tbgb.bml.BMLElement;

/**
 *
 * @author LưuQuang
 */
public class BMLDeserializer implements TBDeserializer
{
    private BMLElement elem;
    
    public BMLDeserializer(BMLElement element)
    {
        this.elem = element;
    }
    
    public void deserialize(String serName, TBSerializable ser) {
        ser.deserialize(this);
    }

    public String deserializeString(String serName) {
        BMLElement subElem = this.elem.getSubElement(serName);
        
        if (subElem != null)
        {
            return subElem.getValue();
        }
        
        return null;
    }

    public int deserializeInteger(String serName) {
        BMLElement subElem = this.elem.getSubElement(serName);
        
        if (subElem != null)
        {
            return Integer.parseInt(subElem.getValue());
        }
        
        return Integer.MIN_VALUE;
    }

    public double deserializeDouble(String serName)
    {
        BMLElement subElem = this.elem.getSubElement(serName);
        
        if (subElem != null)
        {
            return Double.parseDouble(subElem.getValue());
        }
        
        return Double.NaN;
    }

    public Vector deserializeAll(String serName) {
        Vector subElems = this.elem.getAllSubElement(serName);
        Vector subDesers = new Vector(subElems.size());
        
        for (int i = 0; i < subElems.size(); ++i)
        {
            Object subElemObj = subElems.elementAt(i);
            if (subElemObj instanceof BMLElement)
            {
                BMLElement subElem = (BMLElement) subElemObj;
                BMLDeserializer subDeser = new BMLDeserializer(subElem);
                
                subDesers.addElement(subDeser);
            }   
        }
        
        return subDesers;
    }

    public TBDeserializer subDeserializer(String serName) {
        BMLElement subElem = this.elem.getSubElement(serName);
        
        if (subElem != null)
        {
            return new BMLDeserializer(subElem);
        }
        
        return null;
    }

    public boolean hasSub(String subName) {
        BMLElement subElem = this.elem.getSubElement(subName);
        
        return subElem != null;
    }
    
}
