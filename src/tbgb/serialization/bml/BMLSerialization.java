/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.serialization.bml;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import tbgb.serialization.TBDeserializer;
import tbgb.serialization.TBSerialization;
import tbgb.serialization.TBSerializer;
import tbgb.bml.BMLDocument;
import tbgb.bml.BMLDocumentBuilder;

/**
 *
 * @author LưuQuang
 */
public class BMLSerialization implements TBSerialization
{
    public TBSerializer Serialize(OutputStream s) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public TBDeserializer Deserialize(InputStream s) {
        try
        {
            BMLDocument document = BMLDocumentBuilder.getInstance().createDocument(s);
            return new BMLDeserializer(document.getRootElement());
        } catch (IOException ex) {
            return null;
        }
    }

    public void CompleteSerialization(OutputStream s) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
