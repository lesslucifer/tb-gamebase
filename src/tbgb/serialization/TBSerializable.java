package tbgb.serialization;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LưuQuang
 */
public interface TBSerializable {
    void serialize(TBSerializer serializer);
    void deserialize(TBDeserializer deserializer);
}