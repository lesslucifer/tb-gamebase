/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.serialization;

import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author LưuQuang
 */
public interface TBSerialization {
        TBSerializer Serialize(OutputStream s);
        TBDeserializer Deserialize(InputStream s);

        void CompleteSerialization(OutputStream s);
}
