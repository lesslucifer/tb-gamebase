/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.serialization;

import java.util.Vector;

/**
 *
 * @author LưuQuang
 */
public interface TBDeserializer
{
    void deserialize(String serName, TBSerializable ser);
    String deserializeString(String serName);
    int deserializeInteger(String serName);
    double deserializeDouble(String serName);
    Vector deserializeAll(String serName);
    TBDeserializer subDeserializer(String serName);
    
    boolean hasSub(String subName);
}
