/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.resourcemanager;

/**
 *
 * @author LưuQuang
 */
public interface TBResourceLoader
{
    Class getResourceKey();
    boolean isReuseResource();
    
    Object loadResource(String resourceName);
}