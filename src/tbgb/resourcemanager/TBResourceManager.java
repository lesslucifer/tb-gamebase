/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.resourcemanager;

/**
 *
 * @author LưuQuang
 */
public interface TBResourceManager
{
    Object getResource(Class resourceKey, String resourceName);
    Object getResource(String resourceName);

    void addResourceLoader(TBResourceLoader resourceLoader);
}