/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.resourcemanager.statesprite;

import java.util.Hashtable;

/**
 *
 * @author Salm
 */
public class AdHocStateSpriteDataBank extends Hashtable implements StateSpriteDataBank{

    public StateSpriteData getData(String name) {
        Object stSprDataObj = this.get(name);
        if (stSprDataObj instanceof StateSpriteData)
        {
            return (StateSpriteData) stSprDataObj;
        }
        
        return null;
    }
    
}
