/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.resourcemanager.statesprite;

import java.util.Hashtable;

/**
 *
 * @author Salm
 */
public interface StateSpriteData
{
    Hashtable getData();
    int getWidth();
    int getHeight();
}
