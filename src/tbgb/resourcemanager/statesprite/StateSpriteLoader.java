/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.resourcemanager.statesprite;

import java.util.Hashtable;
import tbgb.process.statesprite.StateSprite;
import tbgb.resourcemanager.TBResourceLoader;

/**
 *
 * @author Salm
 */
public class StateSpriteLoader implements TBResourceLoader{
    private StateSpriteDataBank dataBank;
    
    public StateSpriteLoader(StateSpriteDataBank data)
    {
        this.dataBank = data;
    }
    
    public Class getResourceKey() {
        return StateSprite.class;
    }

    public boolean isReuseResource() {
        return false;
    }

    public Object loadResource(String resourceName) {
        StateSpriteData stSprData = this.dataBank.getData(resourceName);
        Hashtable sprites = stSprData.getData();
        int width = stSprData.getWidth();
        int height = stSprData.getHeight();
        
        return new StateSprite(sprites, width, height);
    }
}
