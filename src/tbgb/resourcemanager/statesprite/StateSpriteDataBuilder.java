/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.resourcemanager.statesprite;

import java.util.Hashtable;
import javax.microedition.lcdui.game.Sprite;
import tbgb.resourcemanager.TBResourceManager;
import tbgb.resourcemanager.sprites.SpriteLoader;

/**
 *
 * @author Salm
 */
public class StateSpriteDataBuilder implements StateSpriteData
{
    private TBResourceManager rm;
    private Hashtable sprites = new Hashtable();
    private int width, height;
    
    public StateSpriteDataBuilder(TBResourceManager resourceManager)
    {
        this.rm = resourceManager;
    }
    
    public void addState(int state, String sprName)
    {
        Integer refState = new Integer(state);
        Object sprObj = this.rm.getResource(Sprite.class, sprName);
        if (sprObj instanceof Sprite)
        {
            Sprite sprite = (Sprite) sprObj;
            this.sprites.put(refState, sprite);
            
            width = Math.max(width, sprite.getWidth());
            height = Math.max(height, sprite.getHeight());
        }
    }

    public Hashtable getData()
    {
        return this.sprites;
    }

    public int getWidth()
    {
        return this.width;
    }

    public int getHeight()
    {
        return this.height;
    }
}
