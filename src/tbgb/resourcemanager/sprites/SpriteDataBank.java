/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.resourcemanager.sprites;

/**
 *
 * @author LưuQuang
 */
public interface SpriteDataBank
{
    public static class SpriteData
    {
        public SpriteData()
        {
            
        }
        
        public SpriteData(String img, int width, int height, int refX, int refY, int[] frames)
        {
            this.imageName = img;
            this.width = width;
            this.height = height;
            this.refX = refX;
            this.refY = refY;
            this.frames = frames;
        }
        
        public String imageName;
        public int width, height;
        public int refX, refY;
        public int[] frames;
    }
    
    SpriteData getSpriteData(String spriteName);
}