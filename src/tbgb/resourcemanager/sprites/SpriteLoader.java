/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.resourcemanager.sprites;

import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.Sprite;
import tbgb.resourcemanager.ImageResourceLoader;
import tbgb.resourcemanager.TBResourceLoader;
import tbgb.resourcemanager.TBResourceManager;
import tbgb.resourcemanager.sprites.SpriteDataBank.SpriteData;

/**
 *
 * @author LưuQuang
 */
public class SpriteLoader implements TBResourceLoader{
    public static final Integer RESOURCE_KEY = new Integer("SpriteLoader".hashCode());
    
    private final TBResourceManager resourceManager;
    private final SpriteDataBank dataBank;
    
    public SpriteLoader(TBResourceManager rm, SpriteDataBank data)
    {
        this.resourceManager = rm;
        this.dataBank = data;
    }
    
    public Class getResourceKey() {
        return Sprite.class;
    }

    public boolean isReuseResource() {
        return false;
    }

    public Object loadResource(String resourceName) {
        SpriteData sprData = this.dataBank.getSpriteData(resourceName);
        
        if (sprData != null)
        {
            Object imgObject = this.resourceManager.getResource(Image.class, sprData.imageName);
            if (imgObject instanceof Image)
            {
                Image spriteImage = (Image)imgObject;
                
                Sprite sprite = new Sprite(spriteImage, sprData.width,  sprData.height);
                sprite.setFrameSequence(sprData.frames);
                
                return sprite;
            }
        }
        
        return null;
    }
}