/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.resourcemanager.sprites;

import java.util.Hashtable;
import tbgb.resourcemanager.sprites.SpriteDataBank.SpriteData;

/**
 *
 * @author LưuQuang
 */
public class AdHocSpriteDataBank extends Hashtable implements SpriteDataBank{
    public SpriteData getSpriteData(String spriteName) {
        Object spriteDataObj = this.get(spriteName);
        if (spriteDataObj instanceof SpriteData)
        {
            return (SpriteData) spriteDataObj;
        }
        
        return null;
    }
}
