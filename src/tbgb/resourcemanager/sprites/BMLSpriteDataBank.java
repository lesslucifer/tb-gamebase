/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tbgb.resourcemanager.sprites;

import java.util.Hashtable;
import java.util.Vector;
import tbgb.serialization.TBDeserializer;
import tbgb.serialization.TBSerializable;
import tbgb.serialization.TBSerializer;

/**
 *
 * @author LưuQuang
 */
public class BMLSpriteDataBank implements SpriteDataBank, TBSerializable
{
    private static final String tagSprite = "Sprite";
    private static final String tagName = "Name";
    private static final String tagImage = "Image";
    private static final String tagWidth = "Width";
    private static final String tagHeight = "Height";
    private static final String tagRefX = "RefX";
    private static final String tagRefY = "RefY";
    
    private final Hashtable spriteData = new Hashtable();
    
    public SpriteData getSpriteData(String spriteName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void serialize(TBSerializer serializer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void deserialize(TBDeserializer deserializer) {
        Vector sprites = deserializer.deserializeAll(tagSprite);
        for (int i = 0; i < sprites.size(); ++i)
        {
            Object spriteDeserObject = sprites.elementAt(i);
            if (spriteDeserObject instanceof TBDeserializer)
            {
                TBDeserializer spriteDeser = (TBDeserializer) spriteDeserObject;
                String spriteName = spriteDeser.deserializeString(tagName);
                SpriteData data = this.deserializeData(spriteDeser);
                
                this.spriteData.put(spriteName, data);
            }
        }
    }
    
    private SpriteData deserializeData(TBDeserializer deser)
    {
        SpriteData data = new SpriteData();
        data.imageName = deser.deserializeString(tagImage);
        data.width = deser.deserializeInteger(tagWidth);
        data.height = deser.deserializeInteger(tagHeight);
        if (deser.hasSub(tagRefX))
        {
            data.refX = deser.deserializeInteger(tagRefX);
        }
        if (deser.hasSub(tagRefY))
        {
            data.refY = deser.deserializeInteger(tagRefY);
        }
        
        return data;
    }
}
