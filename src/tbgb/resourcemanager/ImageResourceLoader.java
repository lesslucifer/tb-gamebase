/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.resourcemanager;

import java.io.IOException;
import javax.microedition.lcdui.Image;
import tbgb.log.TBLog;

/**
 *
 * @author LưuQuang
 */
public class ImageResourceLoader implements TBResourceLoader{
    public Class getResourceKey() {
        return Image.class;
    }

    public boolean isReuseResource() {
        return true;
    }

    public Object loadResource(String resourceName) {
        try {
            TBLog.log("Create image: " + resourceName);
            return Image.createImage("/" + resourceName);
        } catch (IOException ex) {
            return null;
        }
    }
}
