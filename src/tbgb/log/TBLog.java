/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tbgb.log;

import java.io.PrintStream;
import java.util.Vector;

/**
 *
 * @author KieuAnh
 */
public class TBLog
{
    public static final int TBLOG_ERROR = -2;
    public static final int TBLOG_WARNING = -1;
    public static final int TBLOG_DEFAULT = 0;
    public static final int TBLOG_DETAIL = 1;
    public static int LOG_LEVEL = TBLOG_DEFAULT;
    
    private static final Vector logVars = new Vector();
    
    public static void logWarning(String s)
    {
        log(TBLOG_WARNING, s, System.err);
    }
    
    public static void logError(String s)
    {
        log(TBLOG_ERROR, s, System.err);
    }
    
    public static void log(String s)
    {
        log(TBLOG_DEFAULT, s, System.out);
    }
    
    public static void logDetail(String s)
    {
        log(TBLOG_DETAIL, s, System.out);
    }
    
    public static void log(int logLevel, String log, PrintStream os)
    {
        if (LOG_LEVEL >= logLevel)
        {
            os.println(log);
        }
    }
}
