/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.bml;

import java.util.Vector;

/**
 *
 * @author LưuQuang
 */
class BMLElementImpl implements BMLElement
{
    private BMLDocumentImpl document;
    private Long currentCode;
    
    public BMLElementImpl(BMLDocumentImpl doc, Long code) {
        this.document = doc;
        this.currentCode = code;
    }
    
    public String getValue()
    {
        return this.document.getValue(currentCode);
    }
    
    public BMLElement getSubElement(String subElemName)
    {
        Long elemCode = this.makeSubElementCode(subElemName, 0);
        return this.document.getElement(elemCode);
    }
    
    public Vector getAllSubElement(String subElemName)
    {
        Vector subElems = new Vector();
        int sibIndex = 0;
        BMLElement elem;
        do
        {
            Long elemCode = this.makeSubElementCode(subElemName, sibIndex);
            elem = this.document.getElement(elemCode);
            
            if (elem != null)
            {
                subElems.addElement(elem);
                ++sibIndex;
            }
        } while (elem != null);
        
        return subElems;
    }
    
    private Long makeSubElementCode(String subElementName, int sibIndex)
    {
        StringBuffer elemCodeStringBuffer = new StringBuffer();
        elemCodeStringBuffer.append(".");
        elemCodeStringBuffer.append(subElementName);
        elemCodeStringBuffer.append("$");
        elemCodeStringBuffer.append(sibIndex);
        String elemCodeString = elemCodeStringBuffer.toString();
        
        int elemOnlyCode = elemCodeString.hashCode();
        return new Long(this.currentCode.longValue() + elemOnlyCode);
    }
}
