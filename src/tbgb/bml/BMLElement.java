/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.bml;

import java.util.Vector;

/**
 *
 * @author LưuQuang
 */
public interface BMLElement {

    Vector getAllSubElement(String subElemName);

    BMLElement getSubElement(String subElemName);

    String getValue();
    
}
