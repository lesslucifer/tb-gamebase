/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.bml;

/**
 *
 * @author LưuQuang
 */
public interface BMLDocument {

    BMLElement getElement(Long code);

    BMLElement getRootElement();

    String getValue(Long code);
    
}
