/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.bml;

import java.util.Hashtable;

/**
 *
 * @author LưuQuang
 */
class BMLDocumentImpl implements BMLDocument
{
    private Hashtable bmlData = new Hashtable();
    private Long rootElementCode;
    
    public BMLDocumentImpl(Hashtable data, Long rootElemCode)
    {
        this.bmlData = data;
        this.rootElementCode = rootElemCode;
    }
    
    public BMLElement getRootElement()
    {
        return new BMLElementImpl(this, rootElementCode);
    }
    
    public String getValue(Long code)
    {
        Object value = this.bmlData.get(code);
        if (value instanceof String)
        {
            return (String) value;
        }
        
        return null;
    }
    
    public BMLElement getElement(Long code)
    {
        if (this.bmlData.containsKey(code))
        {
            return new BMLElementImpl(this, code);
        }
        
        return null;
    }
}
