/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.process.movement;

import tbgb.components.twodimesion.PositionComponent;
import tbgb.components.twodimesion.VelocityComponent;

/**
 *
 * @author LưuQuang
 */
public class LinearMoveProcess implements MoveProcess
{
    private final PositionComponent position;
    private final VelocityComponent velocity;
    
    public LinearMoveProcess(PositionComponent pos, VelocityComponent vel)
    {
        this.position = pos;
        this.velocity = vel;
    }
    
    public void move(long eslapedGameTime) {
        this.position.saAdd(this.velocity.getX(), this.velocity.getY());
    }
}
