/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.process.movement;

import tbgb.BaseGame;
import tbgb.process.BaseGameProcessManager;

/**
 *
 * @author LưuQuang
 */
public class MoveProcessManager extends BaseGameProcessManager{
    public static final int PROCESS_KEY = "MoveProcessManager".hashCode();
    
    public void run(BaseGame game) {
        long gameTime = game.getLoopInterval();
        
        for (int i = 0; i < elementCount; ++i)
        {
            Object moveProcesObject = this.elementAt(i);
            if (moveProcesObject instanceof MoveProcess)
            {
                MoveProcess moveProcess = (MoveProcess) moveProcesObject;
                moveProcess.move(gameTime);
            }
        }
    }

    public boolean addProcess(Object process) {
        if (process instanceof MoveProcess && !this.contains(process))
        {
            this.addElement(process);
            return true;
        }
        
        return false;
    }

    public int getProcessKey() {
        return PROCESS_KEY;
    }
    
}
