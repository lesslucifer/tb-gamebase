/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.process.layer;

import javax.microedition.lcdui.game.Layer;
import javax.microedition.lcdui.game.LayerManager;
import tbgb.BaseGame;
import tbgb.process.GameProcessManager;

/**
 *
 * @author LưuQuang
 */
public class GameLayerManager extends LayerManager implements GameProcessManager
{
    public static final int PROCESS_KEY = "GameLayerManager".hashCode();
    
    private int viewPortX;
    private int viewPortY;
    
    public void run(BaseGame game)
    {
        this.paint(game.getGraphicManager(), viewPortX, viewPortY);
    }

    /**
     * @return the viewPortX
     */
    public int getViewPortX() {
        return viewPortX;
    }

    /**
     * @param viewPortX the viewPortX to set
     */
    public void setViewPortX(int viewPortX) {
        this.viewPortX = viewPortX;
    }

    /**
     * @return the viewPortY
     */
    public int getViewPortY() {
        return viewPortY;
    }

    /**
     * @param viewPortY the viewPortY to set
     */
    public void setViewPortY(int viewPortY) {
        this.viewPortY = viewPortY;
    }
    
    public void setViewPortPosition(int x, int y)
    {
        this.viewPortX = x;
        this.viewPortY = y;
    }
    
    public void moveViewPortPosition(int dx, int dy)
    {
        this.viewPortX += dx;
        this.viewPortY += dy;
    }

    public int getProcessKey() {
        return PROCESS_KEY;
    }

    public boolean addProcess(Object process) {
        if (process instanceof Layer)
        {
            Layer layer = (Layer) process;
            this.append(layer);
            
            return true;
        }
        
        return false;
    }

    public boolean removeProcess(Object process) {
        if (process instanceof Layer)
        {
            Layer layer = (Layer) process;
            this.remove(layer);
            
            return true;
        }
        
        return false;
    }
}
