/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.process.inputhandle;

import tbgb.BaseGame;
import tbgb.process.BaseGameProcessManager;

/**
 *
 * @author LưuQuang
 */
public class InputHandleProcessManager extends BaseGameProcessManager
{
    public static final int PROCESS_KEY = "InputHandleProcessManager".hashCode();
    
    public void run(BaseGame game) {
        int keyStates = game.getKeyStates();
        for (int i = 0; i < this.elementCount; ++i)
        {
            Object inputHandleProcessObject = this.elementAt(i);
            if (inputHandleProcessObject instanceof InputHandleProcess)
            {
                InputHandleProcess inputProcess = (InputHandleProcess) inputHandleProcessObject;
                inputProcess.handleInput(keyStates);
            }
        }
    }

    public boolean addProcess(Object process) {
        if (process instanceof InputHandleProcess && !this.contains(process))
        {
            this.addElement(process);
            
            return true;
        }
        
        return false;
    }

    public int getProcessKey() {
        return PROCESS_KEY;
    }
    
}
