/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.process.statesprite;

import java.util.Enumeration;
import java.util.Hashtable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.ExtendableLayer;
import javax.microedition.lcdui.game.Sprite;
import javax.microedition.lcdui.game.TiledLayer;
import tbgb.components.state.StateComponent;

/**
 *
 * @author Salm
 */
public class StateSprite extends ExtendableLayer{
    private Hashtable sprites = new Hashtable();
    private StateComponent state;
    
    public StateSprite(Hashtable lstSprite, int width, int height)
    {
        super(width, height);
    }
    
    private Sprite getCurrentSprite()
    {
        Object spriteObj = this.sprites.get(state.getState());
        if (spriteObj instanceof Sprite)
        {
            return (Sprite) spriteObj;
        }
        
        return null;
    }
    
    public void setTransform(int i) {
        Enumeration spriteKeys = this.sprites.keys();
        
        while (spriteKeys.hasMoreElements())
        {
            Object spriteObj = this.sprites.get(spriteKeys.nextElement());
            if (spriteObj instanceof Sprite)
            {
                Sprite sprite = (Sprite) spriteObj;
                sprite.setTransform(i);
            }
        }
    }
    
    public boolean collidesWith(StateSprite stSprite, boolean bln)
    {
        Sprite currentSprite = this.getCurrentSprite();
        Sprite otherCurrentSprite = stSprite.getCurrentSprite();
        if (currentSprite != null && otherCurrentSprite != null)
        {
            return currentSprite.collidesWith(otherCurrentSprite, bln);
        }
        
        return false;
    }

    public boolean collidesWith(Sprite sprite, boolean bln) {
        Sprite currentSprite = this.getCurrentSprite();
        if (currentSprite != null)
        {
            return currentSprite.collidesWith(sprite, bln);
        }
        
        return false;
    }

    public boolean collidesWith(TiledLayer tl, boolean bln) {
        Sprite currentSprite = this.getCurrentSprite();
        if (currentSprite != null)
        {
            return currentSprite.collidesWith(tl, bln);
        }
        
        return false;
    }

    public boolean collidesWith(Image image, int i, int i1, boolean bln) {
        Sprite currentSprite = this.getCurrentSprite();
        if (currentSprite != null)
        {
            return currentSprite.collidesWith(image, i, i1, bln);
        }
        
        return false;
    }
    
    public void setPosition(int i, int i1) {
        Enumeration spriteKeys = this.sprites.keys();
        
        while (spriteKeys.hasMoreElements())
        {
            Object spriteObj = this.sprites.get(spriteKeys.nextElement());
            if (spriteObj instanceof Sprite)
            {
                Sprite sprite = (Sprite) spriteObj;
                sprite.setPosition(i, i1);
            }
        }
        
        super.setPosition(i, i1);
    }

    public void move(int i, int i1) {
        Enumeration spriteKeys = this.sprites.keys();
        
        while (spriteKeys.hasMoreElements())
        {
            Object spriteObj = this.sprites.get(spriteKeys.nextElement());
            if (spriteObj instanceof Sprite)
            {
                Sprite sprite = (Sprite) spriteObj;
                sprite.move(i, i1);
            }
        }
        
        super.move(i, i1);
    }

    public void setVisible(boolean bln) {
        Enumeration spriteKeys = this.sprites.keys();
        
        while (spriteKeys.hasMoreElements())
        {
            Object spriteObj = this.sprites.get(spriteKeys.nextElement());
            if (spriteObj instanceof Sprite)
            {
                Sprite sprite = (Sprite) spriteObj;
                sprite.setVisible(bln);
            }
        }
        
        super.setVisible(bln);
    }
    
    public void paint(Graphics grphcs)
    {
        Sprite currentSprite = this.getCurrentSprite();
        if (currentSprite != null)
        {
            currentSprite.paint(grphcs);
        }
    }
}
