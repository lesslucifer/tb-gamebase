/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.process.update;

import tbgb.BaseGame;
import tbgb.process.BaseGameProcessManager;

/**
 *
 * @author LưuQuang
 */
public class UpdateGameProcessManager extends BaseGameProcessManager
{
    public static final int PROCESS_KEY = "UpdateGameProcessManager".hashCode();
    
    public void run(BaseGame game)
    {
        long gameInterval = game.getLoopInterval();
        
        for (int i = 0; i < this.elementCount; ++i)
        {
            UpdateProcess updateable = (UpdateProcess) this.elementAt(i);
            updateable.update(gameInterval);
        }
    }

    public boolean addProcess(Object process) {
        if (process instanceof UpdateProcess && !this.contains(process))
        {
            this.addElement(process);
            return true;
        }
        return false;
    }

    public int getProcessKey() {
        return PROCESS_KEY; 
    }
}
