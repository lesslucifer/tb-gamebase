/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.process.collision;

import java.util.Vector;

/**
 *
 * @author LưuQuang
 */
public interface CollisionProcess
{
    void handleCollision(Vector collisionObjects);
}
