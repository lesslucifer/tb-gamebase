/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.process.collision;

import tbgb.BaseGame;
import tbgb.process.BaseGameProcessManager;

/**
 *
 * @author LưuQuang
 */
public class CollisionProcessManager extends BaseGameProcessManager{
    public static final int PROCESS_KEY = "CollisionProcessManager".hashCode();
    
    public void run(BaseGame game) {
        for (int i = 0; i < this.elementCount; ++i)
        {
            Object collisionProcessObject = this.elementAt(i);
            if (collisionProcessObject instanceof CollisionProcess)
            {
                CollisionProcess collisionProcess = (CollisionProcess) collisionProcessObject;
                collisionProcess.handleCollision(this);
            }
        }
    }

    public boolean addProcess(Object process) {
        if (process instanceof CollisionProcess && !this.contains(process))
        {
            this.addElement(process);
            return true;
        }
        
        return false;
    }

    public int getProcessKey() {
        return PROCESS_KEY;
    }
    
}
