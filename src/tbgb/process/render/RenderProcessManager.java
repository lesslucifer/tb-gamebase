/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.process.render;

import javax.microedition.lcdui.Graphics;
import tbgb.BaseGame;
import tbgb.process.BaseGameProcessManager;

/**
 *
 * @author LưuQuang
 */
public class RenderProcessManager extends BaseGameProcessManager
{
    public static final int PROCESS_KEY = "RenderProcessManager".hashCode();
    
    public void run(BaseGame game) {
        long gameTime = game.getLoopInterval();
        Graphics g = game.getGraphicManager();
        
        for (int i = 0; i < this.elementCount; ++i)
        {
            Object renderProcessObject = this.elementAt(i);
            
            if (renderProcessObject instanceof RenderProcess)
            {
                RenderProcess renderProcess = (RenderProcess) renderProcessObject;
                renderProcess.render(g, gameTime);
            }
        }
    }

    public boolean addProcess(Object process) {
        if (process instanceof RenderProcess && !this.contains(process))
        {
            this.addElement(process);
            
            return true;
        }
        
        return false;
    }

    public int getProcessKey() {
        return PROCESS_KEY;
    }
    
}
