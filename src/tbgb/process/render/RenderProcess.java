/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.process.render;

import javax.microedition.lcdui.Graphics;

/**
 *
 * @author LưuQuang
 */
public interface RenderProcess {
    void render(Graphics g, long eslapedGameTime);
}
