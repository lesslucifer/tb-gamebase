/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.process;

import tbgb.BaseGame;

/**
 *
 * @author LưuQuang
 */
public interface GameProcessManager
{
    int getProcessKey();
    
    boolean addProcess(Object process);
    boolean removeProcess(Object process);
    
    void run(BaseGame game);
}
