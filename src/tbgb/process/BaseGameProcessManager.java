/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.process;

import java.util.Vector;
import tbgb.BaseGame;

/**
 *
 * @author LưuQuang
 */
public abstract class BaseGameProcessManager extends Vector implements GameProcessManager{
            
    public abstract void run(BaseGame game);

    public abstract boolean addProcess(Object process);

    public boolean removeProcess(Object process) {
        return this.removeElement(process);
    }

    public abstract int getProcessKey();
}
