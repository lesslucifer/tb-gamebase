/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tbgb.common;

/**
 *
 * @author KieuAnh
 */
//public class TBNumber
//{
//    public static final float F_MULTIPLY = 256f; 
//    public static final int BIT_MULT = 8;
//    private int val;
//
//    public TBNumber()
//    {
//        val = 0;
//    }
//
//    public TBNumber(TBNumber n)
//    {
//        val = n.val;
//    }
//
//    public TBNumber(int n)
//    {
//        val = n<<BIT_MULT;
//    }
//
//    public TBNumber(float x)
//    {
//        val = (int) (x * F_MULTIPLY);
//    }
//
//
//    public TBNumber copy()
//    {
//        return new TBNumber(this);
//    }
//    
//    public int getRaw()
//    {
//        return val;
//    }
//    
//    public void setRaw(int raw)
//    {
//        val = raw;
//    }
//    
//    public TBNumber negative()
//    {
//        TBNumber res = this.copy();
//        
//        return res.saNegative();
//    }
//    
//    public TBNumber saNegative()
//    {
//        this.val = -this.val;
//        return this;
//    }
//
//    // int
//
//    public int getInt()
//    {
//        return val>>BIT_MULT;
//    }
//
//
//    public TBNumber set(int n)
//    {
//        val = n<<BIT_MULT;
//        return this;
//    }
//
//
//    public TBNumber add(int n)
//    {
//        TBNumber res = this.copy();
//        res.saAdd(n);
//        return res;
//    }
//
//    public TBNumber saAdd(int n)
//    {
//        val += n<<BIT_MULT;
//        return this;
//    }
//
//
//    public TBNumber sub(int n)
//    {
//        TBNumber res = this.copy();
//        res.saSub(n);
//        return res;
//    }
//
//    public TBNumber saSub(int n)
//    {
//        val -= n<<BIT_MULT;
//        return this;
//    }
//
//
//    public TBNumber mult(int n)
//    {
//        TBNumber res = this.copy();
//        res.saMult(n);
//        return res;
//    }
//
//    public TBNumber saMult(int n)
//    {
//        val *= n;
//        return this;
//    }
//
//
//    public TBNumber div(int n)
//    {
//        TBNumber res = this.copy();
//        res.saDiv(n);
//        return res;
//    }
//
//    public TBNumber saDiv(int n)
//    {
//        val /= n;
//        return this;
//    }
//
//
//    public TBNumber mod(int n)
//    {
//        TBNumber res = this.copy();
//        res.saMod(n);
//        return res;
//    }
//
//    public TBNumber saMod(int n)
//    {
//        val >>= BIT_MULT;
//        val %= n;
//        val <<= BIT_MULT;
//        return this;
//    }
//
//    // number
//
//    public TBNumber set(TBNumber n)
//    {
//        val = n.val;
//        return this;
//    }
//
//
//    public TBNumber add(TBNumber n)
//    {
//        TBNumber res = this.copy();
//        res.saAdd(n);
//        return res;
//    }
//
//    public TBNumber saAdd(TBNumber n)
//    {
//        val += n.val;
//        return this;
//    }
//
//
//    public TBNumber sub(TBNumber n)
//    {
//        TBNumber res = this.copy();
//        res.saSub(n);
//        return res;
//    }
//
//    public TBNumber saSub(TBNumber n)
//    {
//        val -= n.val;
//        return this;
//    }
//
//
//    public TBNumber mult(TBNumber n)
//    {
//        TBNumber res = this.copy();
//        res.saMult(n);
//        return res;
//    }
//
//    public TBNumber saMult(TBNumber n)
//    {
//        val *= n.val>>BIT_MULT;
//        return this;
//    }
//
//
//    public TBNumber div(TBNumber n)
//    {
//        TBNumber res = this.copy();
//        res.saDiv(n);
//        return res;
//    }
//
//    public TBNumber saDiv(TBNumber n)
//    {
//        val /= n.val>>BIT_MULT;
//        return this;
//    }
//
//
//    public TBNumber mod(TBNumber n)
//    {
//        TBNumber res = this.copy();
//        res.saMod(n);
//        return res;
//    }
//
//    public TBNumber saMod(TBNumber n)
//    {
//        val %= n.val>>BIT_MULT;
//        return this;
//    }
//
//    // float
//
//    public float getFloat()
//    {
//        return ((float) val) / F_MULTIPLY;
//    }
//
//
//    public TBNumber set(float n)
//    {
//        val = (int)(n * F_MULTIPLY)<<BIT_MULT;
//        return this;
//    }
//
//
//    public TBNumber add(float n)
//    {
//        TBNumber res = this.copy();
//        res.saAdd(n);
//        return res;
//    }
//
//    public TBNumber saAdd(float n)
//    {
//        val += (int) (n * F_MULTIPLY)<<BIT_MULT;
//        return this;
//    }
//
//
//    public TBNumber sub(float n)
//    {
//        TBNumber res = this.copy();
//        res.saSub(n);
//        return res;
//    }
//
//    public TBNumber saSub(float n)
//    {
//        val -= (int) (n * F_MULTIPLY)<<BIT_MULT;
//        return this;
//    }
//
//
//    public TBNumber mult(float n)
//    {
//        TBNumber res = this.copy();
//        res.saMult(n);
//        return res;
//    }
//
//    public TBNumber saMult(float n)
//    {
//        val *= n;
//        return this;
//    }
//
//
//    public TBNumber div(float n)
//    {
//        TBNumber res = this.copy();
//        res.saDiv(n);
//        return res;
//    }
//
//    public TBNumber saDiv(float n)
//    {
//        val /= n;
//        return this;
//    }
//
//    // category - set
//}
