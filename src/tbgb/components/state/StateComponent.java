/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.components.state;

/**
 *
 * @author Salm
 */
public interface StateComponent
{
    public static final int COMPONENT_KEY = "StateComponent".hashCode();
    
    Integer getState();
    void setState(Integer state);
}