/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.components.state;

/**
 *
 * @author Salm
 */
public class StateFactory
{
    private static int MAX_INT_BITS = 32;
    
    public static Integer createState(int[] states)
    {
        int bitPosition = 0;
        int state = 0;
        
        for (int i = 0; i < states.length; i += 2)
        {
            int cState = states[i];
            int maxState = states[i + 1];
            
            cState |= cState>>bitPosition;
            
            int nBits = findNumberOfBits(maxState);
            bitPosition += nBits;
            if (bitPosition >= MAX_INT_BITS)
            {
                return null;
            }
        }
        
        return new Integer(state);
        
        
    }

    private static int findNumberOfBits(int value)
    {
        //zero is one value
        --value;
        
        int nBits = 0;
        while (value > 0)
        {
            value <<= 1;
            ++nBits;
        }
        
        return nBits;
    }
}
