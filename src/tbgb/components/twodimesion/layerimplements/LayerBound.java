/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.components.twodimesion.layerimplements;

import javax.microedition.lcdui.game.Layer;
import tbgb.components.twodimesion.BoundComponent;
import tbgb.components.twodimesion.PositionComponent;
import tbgb.components.twodimesion.SizeComponent;

/**
 *
 * @author LưuQuang
 */
public class LayerBound implements BoundComponent, PositionComponent, SizeComponent
{
    private final Layer layer;
    
    public LayerBound(Layer layer)
    {
        this.layer = layer;
    }
    
    public float getLeft() {
        return this.layer.getX();
    }

    public float getRight() {
        return this.layer.getX() + this.layer.getWidth();
    }

    public float getTop() {
        return this.layer.getY();
    }

    public float getBottom() {
        return this.layer.getY() + this.layer.getHeight();
    }

    public void setLeft(float left) {
        this.layer.setPosition((int) left, this.layer.getY());
    }

    public void setRight(float right) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setTop(float top) {
        this.layer.setPosition(this.layer.getX(), (int) top);
    }

    public void setBottom(float bottom) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public float getX() {
        return this.layer.getX();
    }

    public float getY() {
        return this.layer.getY();
    }

    public void setX(float x) {
        this.layer.setPosition((int) x, this.layer.getY());
    }

    public void setY(float y) {
        this.layer.setPosition(this.layer.getX(), (int) y);
    }

    public void saAdd(float dx, float dy) {
        this.layer.setPosition(this.layer.getX() + (int) dx, this.layer.getY() + (int) dy);
    }

    public float getWidth() {
        return this.layer.getWidth();
    }

    public float getHeight() {
        return this.layer.getHeight();
    }

    public void setWidth(float width) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setHeight(float height) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public float getSquaredLength() {
        return this.getWidth() * this.getWidth() + this.getHeight() * this.getHeight();
    }

    public PositionComponent getPositionComponent() {
        return this;
    }

    public SizeComponent getSizeComponent() {
        return this;
    }

    public float getCenterX() {
        return (this.getRight()- this.getLeft()) / 2;
    }

    public float getCenterY() {
        return (this.getBottom() - this.getTop()) / 2;
    }

    public void setCenterX(float cX) {
        this.setLeft(cX + this.getWidth() / 2);
    }

    public void setCenterY(float cY) {
        this.setTop(cY + this.getHeight()/ 2);
    }
}
