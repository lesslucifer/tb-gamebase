/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.components.twodimesion.point2dimplements;

import tbgb.components.twodimesion.SizeComponent;

/**
 *
 * @author LưuQuang
 */
public class Point2DSize extends Point2D implements SizeComponent
{
    public float getWidth() {
        return this.X;
    }

    public float getHeight() {
        return this.Y;
    }

    public void setWidth(float width) {
        this.X = width;
    }

    public void setHeight(float height) {
        this.Y = height;
    }

    public float getSquaredLength() {
        return X * X + Y * Y;
    }
    
}
