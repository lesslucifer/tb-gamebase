/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.components.twodimesion.point2dimplements;

import tbgb.components.twodimesion.TwoDimesionComponent;

/**
 *
 * @author LưuQuang
 */
class Point2DTwoDimension extends Point2D implements TwoDimesionComponent
{
    public float getX() {
        return this.X;
    }

    public float getY() {
        return this.Y;
    }

    public void setX(float x) {
        this.X = x;
    }

    public void setY(float y) {
        this.Y = y;
    }
}
