/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.components.twodimesion.point2dimplements;

import tbgb.components.twodimesion.Rectangle;

/**
 *
 * @author LưuQuang
 */
class Point2DRectangle extends Rectangle{
    public Point2DRectangle()
    {
        super(new Point2DPosition(), new Point2DSize());
    }
}
