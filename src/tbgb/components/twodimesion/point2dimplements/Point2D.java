/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.components.twodimesion.point2dimplements;

/**
 *
 * @author LưuQuang
 */
public class Point2D {
    public static final Point2D ZERO = new Point2D();
    
    public float X, Y;
    
    public Point2D()
    {
        
    }
    
    public Point2D(float x, float y)
    {
        this.X = x;
        this.Y = y;
    }
    
    public Point2D(Point2D p)
    {
        this.X = p.X;
        this.Y = p.Y;
    }
            
    public void set(Point2D p)
    {
        this.X = p.X;
        this.Y = p.Y;
    }
    
    public void saAdd(float dx, float dy)
    {
        X += dx;
        Y += dy;
    }
    
    public void saMult(float n)
    {
        X *= n;
        Y *= n;
    }
    
    public void toZero()
    {
        this.X = 0;
        this.Y = 0;
    }
    
    public boolean isZero()
    {
        return (0f == X && 0f == Y);
    }
    
    public void set(float x, float y)
    {
        this.X = x;
        this.Y = y;
    }
    
    public void normalize()
    {
        
    }
    
    public float getSquaredLength()
    {
        return X*X + Y*Y;
    }
    
    public String toString()
    {
        return "{" + String.valueOf(X) + "-" + String.valueOf(Y) + "}";
    }
}
