/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.components.twodimesion.point2dimplements;

import tbgb.components.twodimesion.BoundComponent;
import tbgb.components.twodimesion.PositionComponent;
import tbgb.components.twodimesion.SizeComponent;
import tbgb.components.twodimesion.TwoDimensionFactory;
import tbgb.components.twodimesion.VelocityComponent;

/**
 *
 * @author LưuQuang
 */
public class Point2DFactory implements TwoDimensionFactory{
    private Point2DFactory() {
        
    }
    private static final Point2DFactory inst = new Point2DFactory();
    
    public static Point2DFactory getInstance() {
        return inst;
    }

    public Point2D createPoint2D() {
        return new Point2D();
    }

    public Point2D createPoint2D(int x, int y) {
        Point2D point = new Point2D();
        point.X = x;
        point.Y = y;
        
        return point;
    }

    public PositionComponent createPositionComponent() {
        return new Point2DPosition();
    }

    public PositionComponent createPositionComponent(int x, int y) {
        PositionComponent position = new Point2DPosition();
        position.setX(x);
        position.setY(y);
        
        return position;
    }

    public VelocityComponent createVelocityComponent() {
        return new Point2DVelocity();
    }

    public VelocityComponent createVelocityComponent(int x, int y) {
        VelocityComponent velocity = new Point2DVelocity();
        velocity.setX(x);
        velocity.setY(y);
        
        return velocity;
    }

    public SizeComponent createSizeComponent() {
        return new Point2DSize();
    }

    public SizeComponent createSizeComponent(int width, int height) {
        SizeComponent size = new Point2DSize();
        size.setWidth(width);
        size.setHeight(height);
        
        return size;
    }

    public BoundComponent createBoundComponent() {
        return new Point2DRectangle();
    }
}
