/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.components.twodimesion;

/**
 *
 * @author LưuQuang
 */
public interface SizeComponent{
    public static final Integer COMPONENT_KEY = new Integer("SizeComponent".hashCode());
    float getWidth();
    float getHeight();
    
    void setWidth(float width);
    void setHeight(float height);
    
    void saAdd(float dWidth, float dHeight);
    float getSquaredLength();
}
