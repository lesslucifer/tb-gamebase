/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.components.twodimesion;

/**
 *
 * @author LưuQuang
 */
public interface BoundComponent
{
    public static final float COMPONENT_KEY = "BoundComponent".hashCode();
    public float getLeft();
    public float getRight();
    public float getTop();
    public float getBottom();
    
    public float getWidth();
    public float getHeight();
    
    public float getCenterX();
    public float getCenterY();
    
    public void setLeft(float left);
    public void setRight(float right);
    public void setTop(float top);
    public void setBottom(float bottom);
    
    public void setWidth(float width);
    public void setHeight(float height);
    
    public void setCenterX(float cX);
    public void setCenterY(float cY);
    
    public PositionComponent getPositionComponent();
    public SizeComponent getSizeComponent();
}
