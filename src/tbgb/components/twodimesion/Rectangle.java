/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.components.twodimesion;

/**
 *
 * @author LưuQuang
 */
public class Rectangle implements BoundComponent{
    public PositionComponent Position;
    public SizeComponent Size;
    
    public Rectangle(PositionComponent positionComponent, SizeComponent sizeComponent)
    {
        this.Position = positionComponent;
        this.Size = sizeComponent;
    }
    
    public float getX()
    {
        return Position.getX();
    }
    
    public float getY()
    {
        return Position.getY();
    }
    
    public void setX(float x)
    {
        this.Position.setX(x);
    }
    
    public void setY(float y)
    {
        this.Position.setY(y);
    }
    
    public void add(float dx, float dy)
    {
        this.Position.saAdd(dx, dy);
    }
    
    public float getWidth()
    {
        return this.Size.getWidth();
    }
    
    public float getHeight()
    {
        return this.Size.getHeight();
    }
    
    public void setWidth(float width)
    {
        this.Size.setWidth(width);
    }
    
    public void setHeight(float height)
    {
        this.Size.setHeight(height);
    }
    
    public float getLeft()
    {
        return this.Position.getX();
    }
    
    public float getRight()
    {
        return this.Position.getX() + this.Size.getWidth();
    }
    
    public float getTop()
    {
        return this.Position.getY();
    }
    
    public float getBottom()
    {
        return this.Position.getY() + this.Size.getHeight();
    }
    
    public void setLeft(float left)
    {
        this.Position.setX(left);
    }
    
    public void setRight(float right)
    {
        this.Size.setWidth(right - this.Position.getX());
    }
    
    public void setTop(float top)
    {
        this.Position.setY(top);
    }
    
    public void setBottom(float bottom)
    {
        this.Size.setHeight(bottom - this.Position.getY());
    }

    public float getSquaredLength() {
        return this.Size.getSquaredLength();
    }

    public PositionComponent getPositionComponent() {
        return this.Position;
    }

    public SizeComponent getSizeComponent() {
        return this.Size;
    }

    public float getCenterX() {
        return (this.getRight()- this.getLeft()) / 2;
    }

    public float getCenterY() {
        return (this.getBottom() - this.getTop()) / 2;
    }

    public void setCenterX(float cX) {
        this.setLeft(cX + this.getWidth() / 2);
    }

    public void setCenterY(float cY) {
        this.setTop(cY + this.getHeight()/ 2);
    }
}