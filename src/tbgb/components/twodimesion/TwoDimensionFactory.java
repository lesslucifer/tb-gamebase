/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.components.twodimesion;

/**
 *
 * @author LưuQuang
 */
public interface TwoDimensionFactory
{
    public PositionComponent createPositionComponent();
    public PositionComponent createPositionComponent(int x, int y);
    
    public VelocityComponent createVelocityComponent();
    public VelocityComponent createVelocityComponent(int x, int y);
    
    public SizeComponent createSizeComponent();
    public SizeComponent createSizeComponent(int width, int height);
    
    public BoundComponent createBoundComponent();
}
