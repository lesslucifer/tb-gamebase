/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.components.twodimesion;

/**
 *
 * @author LưuQuang
 */
public interface TwoDimesionComponent {
    float getX();
    float getY();
    
    void setX(float x);
    void setY(float y);
    
    public void saAdd(float dx, float dy);
}
