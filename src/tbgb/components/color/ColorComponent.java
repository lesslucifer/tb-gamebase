/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.components.color;

import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Salm
 */
public class ColorComponent
{
    public static final int COMPONENT_KEY = "ColorComponent".hashCode();
    
    private int color;
    
    public ColorComponent()
    {
    }
    
    public ColorComponent(int color)
    {
        this.color = color;
    }

    /**
     * @return the color
     */
    public int getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(int color) {
        this.color = color;
    }
    
    public byte getA()
    {
        return (byte) (color>>24);
    }
    
    public void setA(byte a)
    {
        int iA = a;
        color &= 0x00ffffff;
        color |= iA<<24;
    }
    
    public byte getR()
    {
        return (byte) (color<<8>>24);
    }
    
    public void setR(byte r)
    {
        int iR = r;
        color &= 0xff00ffff;
        color |= iR<<16;
    }
    
    public byte getG()
    {
        return (byte) (color<<16>>24);
    }
    
    public void setG(byte g)
    {
        int iG = g;
        color &= 0xffff00ff;
        color |= iG<<8;
    }
    
    public byte getB()
    {
        return (byte) (color<<24>>24);
    }
    
    public void setB(byte b)
    {
        int iB = b;
        color &= 0xfffff00;
        color |= iB;
    }
    
    public static int createColor(byte r, byte g, byte b, byte a)
    {
        return a<<24 | r<<16 | g<<8 | b;
    }
    
    public static final int WHITE = 0xffffffff;
    public static final int BLACK = 0xff000000;
    public static final int RED = 0xffff0000;
    public static final int GREEN = 0xff00ff00;
    public static final int BLUE = 0xff0000ff;
    public static final int TRANSPARENT = 0x00000000;
}
