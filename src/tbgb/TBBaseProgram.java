/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb;

import javax.microedition.lcdui.Display;
import javax.microedition.midlet.MIDlet;

/**
 *
 * @author LưuQuang
 */
public abstract class TBBaseProgram extends MIDlet{
    private BaseGame game;
    private final Thread gameThread;
    
    public TBBaseProgram()
    {
        super();
        
        game = this.createGame();
        game.initialize();
        
        gameThread = new Thread(game);
    }
    
    protected abstract BaseGame createGame();
    
    public void startApp()
    {
        Display currentDisplay = Display.getDisplay(this);
        currentDisplay.setCurrent(game);
        
        gameThread.start();
    }
    
    public void pauseApp() {
        game.pause();
    }
    
    public void destroyApp(boolean unconditional) {
        game.onExit();
    }
}
