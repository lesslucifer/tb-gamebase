/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tbgb.utils.collection;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 *
 * @author LưuQuang
 */
public class TracedHashtable extends Hashtable
{
    public TracedHashtable()
    {
        super();
    }
    
    public TracedHashtable(int cap)
    {
        super(cap);
    }
    
    private boolean isTracing = false;
    private final Vector tracedStorage = new Vector();
    
    public void beginTrace()
    {
        this.isTracing = true;
        this.tracedStorage.removeAllElements();
        
        Enumeration allKeys = this.keys();
        while (allKeys.hasMoreElements())
        {
            Object key = allKeys.nextElement();
            this.tracedStorage.addElement(key);
        }
    }

    public void endTrace()
    {
        this.isTracing = false;

        for (int i = 0; i < this.tracedStorage.size(); ++i)
        {
            Object key = this.tracedStorage.elementAt(i);
            this.remove(key);
        }
    }
    
    public void unmark(Object k)
    {
        if (this.isTracing)
        {
            this.tracedStorage.addElement(k);
        }
    }
    
    public Object getAndMarkTraced(Object key)
    {
        if (this.isTracing && this.tracedStorage.contains(key))
        {
            this.tracedStorage.removeElement(key);
        }

        return this.get(key);
    }
}
