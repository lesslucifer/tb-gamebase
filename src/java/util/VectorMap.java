/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package java.util;

/**
 *
 * @author KieuAnh
 */
public class VectorMap
{
    private final Vector keys;
    private final Vector values;
    
    public VectorMap()
    {
        keys = new Vector();
        values = new Vector();
    }
    
    public VectorMap(int cap)
    {
        keys = new Vector(cap);
        values = new Vector(cap);
    }
    
    public VectorMap(int cap, int incr)
    {
        keys = new Vector(cap, incr);
        values = new Vector(cap, incr);
    }
    
    public Object get(Object key)
    {
        int i = this.keys().indexOf(key);
        if (i > 0)
        {
            return this.values().elementAt(i);
        }
        
        return null;
    }
    
    public void put(Object key, Object val)
    {
        int i = this.keys().indexOf(key);
        if (i > 0)
        {
            this.values().setElementAt(val, i);
        }
        else
        {
            this.add(key, val);
        }
    }

    public void add(Object key, Object val)
    {
        this.keys().addElement(key);
        this.values().addElement(val);
    }
    
    public void insertAtIndex(Object key, Object val, int i)
    {
        this.keys().insertElementAt(key, i);
        this.values().insertElementAt(val, i);
    }
    
    public boolean containsKey(Object key)
    {
        return this.keys().contains(key);
    }
    
    public void remove(Object _key)
    {
        for (int i = 0; i < keys.size();)
        {
            Object key = keys.elementAt(i);
            if (key.equals(_key))
            {
                this.keys.removeElementAt(i);
                this.values.removeElementAt(i);
            }
            else
            {
                ++i;
            }
        }
    }
    
    public void removeAll()
    {
        this.keys.removeAllElements();
        this.values.removeAllElements();
    }

    /**
     * @return the keys
     */
    public Vector keys() {
        return keys;
    }

    /**
     * @return the values
     */
    public Vector values() {
        return values;
    }
}
